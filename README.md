# Project: Goldfinger Vision Respitory
Consists of 2 packages

*	ai_server: For coordinate finding through AI and / or image processing	
*	pcl_server: For point cloud / depth processing and data access

### Launching 
1. Launch all 4 cameras, with pcl_server 
```roslaunch ai_server roboception.launch```
2. Launch ai_server, tensorflow is required
```roslaunch ai_server ai.launch```
---
## ai_server
#### About AI Models
It is not good to upload the whole AI models to remote respitory, hence the model folder will be excluded, please download the latest AI models from Teams or Onedrive and put them under "/ai_server/config/*(module names)*/"
You should have 2 folders and 1 file under it, namely: *checkpoint, saved_model* and *pipeline.config* respectively.

Remember to include the correct version of label_map.pbtxt udner the same directory as well.

### Individual Launching
1. Install CUDA, cuDNN, and tensorflow (2.0 or above) based on your system
2. ```rosrun ai_server ai_master_node.py```

#### About action_lib usage
AIServerGoal has 8 arguments:

* int8 AIServerGoal.task - Set the task parameter to run different functions, possible task:
	* 0 or not set
		* Default function, activate targeted camera and output the ai result of the image
	* int8 AIServerGoal.PLACE_SUCCESS_CHECK (10)
		* Check if the *instrument_needed* is seen by the ai
	* int8 AIServerGoal.PICK_SUCCESS_CHECK (11)
		* Check if the item of the following properties *previous_tried_inst_u, previous_tried_inst_v* and *previous_tried_inst_name* are picked
	* int8 AIServerGoal.FORCE_ISOLATION (2)
		* For PT1 Usage only, to forcefully perform instrument separation, mainly due to inspection bay availability
	* int8 AIServerGoal.PICK_FROM_FLIP (3)
		* For PT1 Usage only, to exclude all ai result outside the flipping mechanism area and pick the item on it
	* int8 AIServerGoal.GET_BOUNDARY_CENTER (4)
		* For PT1 Usage only, returns the center coordinate of the rotating table based on Aruco marker seen
	* int8 AIServerGoal.CALIBRATE_EXP_WB (7)
		* For PT1 Usage only, calibrate the camera white balance based on the rotating table vertices
* int8 AIServerGoal.which_module - Set the module parameter to tell the system which camera is of interest
	* int8 AIServerGoal.WETBAY (61)
	* int8 AIServerGoal.DRYBAY (62)
	* int8 AIServerGoal.PT1 (63)
	* int8 AIServerGoal.PT2 (64)
* string AIServerGoal.instrument_needed - Set the name of the instrument of interest, used together with PLACE_SUCCESS_CHECK or Default task cases
* int16 AIServerGoal.previous_tried_inst_u - Set the x-axis coordinate (u) of the previously tried instrument, used together with PICK_SUCCESS_CHECK task case 
* int16 AIServerGoal.previous_tried_inst_v - Set the y-axis coordinate (v) of the previously tried instrument, used together with PICK_SUCCESS_CHECK task case 
* string AIServerGoal.previous_tried_inst_name - Set the name of the previously tried instrument, used together with PICK_SUCCESS_CHECK task case 
* inspection_bay right_ib - Special message type to let the system knows the state of right inspection bay, consists of 3 elements:
	* bool ready - availability of the inspection bay
	* int32 time_last_load - number of sec since the last time insrtument is loaded into the inspection bay
	* string inst_last_load - name of the instrument last loaded into the inspection bay
* inspection_bay left_ib - Special message type to let the system knows the state of left inspection bay, consists of 3 elements:
	* bool ready - availability of the inspection bay
	* int32 time_last_load - number of sec since the last time insrtument is loaded into the inspection bay
	* string inst_last_load - name of the instrument last loaded into the inspection bay	
		
		
		
---
## pcl_server
The PCL library is responsible of getting depth data from cameras and return to its caller. This can be done by calling via action_lib, PCLServerGoal.

### Individual Launching
```rosrun pcl_server pcl_server```

#### About action_lib usage
PCLServerGoal has 3 arguments:

* int8 PCLServerGoal.task - Set the task parameter to run different functions, possible task:
	* int8 PCLServerGoal.UPDATE_CLOUD (1)
		* Activate the camera and update the point cloud data stored in the memory (flashlight will fire)
	* int8 PCLServerGoal.GET_COOR (2)
		* Based on the previous point cloud data and the pcl_input, return the a world frame based coordinate
		
* string PCLServerGoal.module - Set the module parameter to tell the system which camera is of interest
	* possible parameter: "wetbay", "pt1", "pt2" and "drybay"
	
* int8[3] PCLServerGoal.pcl_input - Set the pcl_input parameter to tell the system the pixel and orientation of interest for transformation
	* \[u, v, theta] of the object of interest.