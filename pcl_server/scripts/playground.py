#!/usr/bin/env python
import rospy
from sensor_msgs.msg import PointCloud2
from pcl_server.msg import *
import actionlib

pt1_sub, wetbay_sub = None, None

class PCLServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('pcl_server', PCLServerAction, self.execute, False)
    self.server.start()
    print("server_connected")
  
  # def pt1_callback(self, data):
  #   print("got wetbay data")
  #   # self.pt1_sub.unregister()

  # def wetbay_callback(self, data):
  #   print("got wetbay data")
  #   # self.wetbay_sub.unregister()

  def execute(self, goal):
    print("service called")
    print(goal)
    if goal.module == "pt1":
      self.pt1_sub = rospy.Subscriber('/pt1/stereo/points2', PointCloud2, self.pt1_callback)
    elif goal.module == "wetbay":
      self.wetbay_sub = rospy.Subscriber('/wetbay/stereo/points2', PointCloud2, self.wetbay_callback)
    self.server.set_succeeded()
    print("ended")
    

def pt1_callback(data):
  global pt1_sub
  print("got wetbay data")
  print(data.height, data.width)
  pt1_sub.unregister()

def wetbay_callback(data):
  global wetbay_sub
  print("got wetbay data")
  print(data.height, data.width)
  wetbay_sub.unregister()

def main():
  global pt1_sub, wetbay_sub
  pt1_sub = rospy.Subscriber('/pt1/stereo/points2', PointCloud2, pt1_callback)
  #wetbay_sub = rospy.Subscriber('/wetbay/stereo/points2', PointCloud2, wetbay_callback)

if __name__ == "__main__":
  rospy.init_node('pcl_server', anonymous=True)
  server = PCLServer()
  main()
  rospy.spin()
