#!/usr/bin/env python
import rospy
import os
if __name__ == "__main__":
  rospy.init_node('pcl_server', anonymous=True)
  server = PCLServer()
  main()
  rospy.spin()
