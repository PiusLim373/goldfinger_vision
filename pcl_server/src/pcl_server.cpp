#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2_msgs/TFMessage.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <math.h>
#include <actionlib/server/simple_action_server.h>
#include <pcl_server/PCLServerAction.h>
#include <rc_common_msgs/Trigger.h>
#include <iostream>
#include <string>
#include <chrono>

using namespace std;

typedef actionlib::SimpleActionServer<pcl_server::PCLServerAction> Server;

ros::ServiceClient wb_pcl_client;
ros::ServiceClient pt1_pcl_client;
ros::ServiceClient db_pcl_client;

bool CLOUD_UPDATED = false;



// function to convert camera based posestamped to world base posestamped
geometry_msgs::PoseStamped transform_frame(geometry_msgs::PoseStamped Pose, string module){
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);
    cout<< "looking up " << module << "_camera" << endl;
    geometry_msgs::TransformStamped transform_stamped = tfBuffer.lookupTransform(module+"_camera", "world", ros::Time(0), ros::Duration(1.0));
    cout << transform_stamped <<endl;
    geometry_msgs::PoseStamped TransformedPose;
    tfBuffer.transform(Pose, TransformedPose, "world");
    return(TransformedPose);
}


pcl::PointCloud<pcl::PointXYZ> pt1_cloud;
pcl::PointCloud<pcl::PointXYZ> wetbay_cloud;
pcl::PointCloud<pcl::PointXYZ> drybay_cloud;
tf2_msgs::TFMessage tf_static;


void executeCB(const pcl_server::PCLServerGoalConstPtr &goal, Server* as)
{
    pcl_server::PCLServerResult result;
    if (goal->task == goal->UPDATE_POINTCLOUD){
        auto begin_time = chrono::system_clock::now();
        sensor_msgs::PointCloud2Ptr cloud_msg;
        rc_common_msgs::Trigger srv;
        if (goal->module.compare("wetbay") == 0){
            cout<< "updating wetbay pointcloud" << endl;
            CLOUD_UPDATED = false;
            wb_pcl_client.call(srv);
            cout << "service called successfully, waiting for cloud getting updated" <<endl;
            while (!CLOUD_UPDATED){
                auto end_time = chrono::system_clock::now();
                std::chrono::duration<double> diff = end_time - begin_time;
                if(diff.count() > 3.0) break;
            }
            if (!CLOUD_UPDATED){
                cout << "timeout!" << endl;
                result.success = false;
            } 
            else result.success = true;
            as->setSucceeded(result);
            return;
        }
        else if (goal->module.compare("pt1") == 0){
            cout<< "updating pt1 cloud" << endl;
            CLOUD_UPDATED = false;
            pt1_pcl_client.call(srv);
            cout << "service called successfully, waiting for cloud getting updated" <<endl;
            clock_t end_time = clock();
            while (!CLOUD_UPDATED){
                auto end_time = chrono::system_clock::now();
                std::chrono::duration<double> diff = end_time - begin_time;
                if(diff.count() > 3.0) break;
            }
            if (!CLOUD_UPDATED){
                cout << "timeout!" << endl;
                result.success = false;
            } 
            else result.success = true;
            as->setSucceeded(result);
            return;
        }
        else if (goal->module.compare("drybay") == 0){
            cout<< "updating drybay cloud" << endl;
            CLOUD_UPDATED = false;
            db_pcl_client.call(srv);
            cout << "service called successfully, waiting for cloud getting updated" <<endl;
            while (!CLOUD_UPDATED){
                auto end_time = chrono::system_clock::now();
                std::chrono::duration<double> diff = end_time - begin_time;
                if(diff.count() > 3.0) break;
            }
            if (!CLOUD_UPDATED){
                cout << "timeout!" << endl;
                result.success = false;
            } 
            else result.success = true;
            as->setSucceeded(result);
            return;
        }
        else{
            cout<< "invalid module" << endl;
            result.success = false;
            as->setSucceeded(result);
            return;
        }

        /*** 
        To include a fix where dont directly subscribe to the pointcloud2 topic to prevent pooling, so far tried using this function but seems like need to move the waitForMessage function into a thread such that the srv call can run simultaneously
        sensor_msgs::PointCloud2ConstPtr cloud_data  = ros::topic::waitForMessage<sensor_msgs::PointCloud2>("/wetbay/stereo/points2", ros::Duration(3.0));
        pcl::fromROSMsg(*cloud_msg, pt1_cloud);
        ***/
        
    }
    else if(goal->task == goal->GET_COOR){
        // ROS_INFO("I heard %d", goal->ai_output[0]);
        geometry_msgs::PoseStamped coor_wrt_camera;
        geometry_msgs::PoseStamped coor_wrt_world;
        pcl_server::PCLServerResult result;
        int u = goal->pcl_input[0];
        int v = goal->pcl_input[1];
        int theta = goal->pcl_input[2];
        cout << goal-> module << endl;
        pcl::PointXYZ point;
        if (goal->module.compare("wetbay") == 0){
            cout<< "checking wetbay cloud" << endl;
            try{
                point = wetbay_cloud.at(u, v);
            }
            catch(pcl::IsNotDenseException& err){
                cout<< "cloud not valid, have you get depth data before?" << err.what() << endl;
                result.success = false;
                as->setSucceeded(result);
                return;
            }
        }
        else if (goal->module.compare("pt1") == 0){
            cout<< "checking pt1 cloud" << endl;
            try{
                point = pt1_cloud.at(u, v);
            }
            catch(pcl::IsNotDenseException& err){
                cout<< "cloud not valid, have you get depth data before?" << err.what() << endl;
                result.success = false;
                as->setSucceeded(result);
                return;
            }
        }
        else if (goal->module.compare("drybay") == 0){
            cout<< "checking drybay cloud" << endl;
            try{
                point = drybay_cloud.at(u, v);
            }
            catch(pcl::IsNotDenseException& err){
                cout<< "cloud not valid, have you get depth data before?" << err.what() << endl;
                result.success = false;
                as->setSucceeded(result);
                return;
            }
        }
        
        else{
            cout<< "invalid module" << endl;
            result.success = false;
            as->setSucceeded(result);
            return;
        }
        ROS_INFO("x: [%f]; y: [%f]; z: [%f]", point.x, point.y, point.z);
        if (isnan(point.x) || isnan(point.y) || isnan(point.z)){
            ROS_INFO("couldn't determine depth");
            result.success = false;
            as->setSucceeded(result);
            return;
        }
        else{
            coor_wrt_camera.header.stamp = ros::Time::now();
            coor_wrt_camera.header.frame_id = goal-> module +"_camera";
            coor_wrt_camera.pose.position.x = point.x;
            coor_wrt_camera.pose.position.y = point.y;
            coor_wrt_camera.pose.position.z = point.z;
            tf2::Quaternion quat;

            quat.setRPY(0, 0, (-90 + theta) * M_PI / 180);
            coor_wrt_camera.pose.orientation.x = quat.x();
            coor_wrt_camera.pose.orientation.y = quat.y();
            coor_wrt_camera.pose.orientation.z = quat.z();
            coor_wrt_camera.pose.orientation.w = quat.w();

            coor_wrt_world = transform_frame(coor_wrt_camera, goal->module);
            
            //VERIFY IF THIS WORKS
            quat.setRPY(180* M_PI / 180, 0, (-(-90 + theta)+180) * M_PI / 180);
            coor_wrt_world.pose.orientation.x = quat.x();
            coor_wrt_world.pose.orientation.y = quat.y();
            coor_wrt_world.pose.orientation.z = quat.z();
            coor_wrt_world.pose.orientation.w = quat.w();

            result.coor_wrt_world = coor_wrt_world;
            cout << coor_wrt_world;
            result.success = true;
            as->setSucceeded(result);
            return;
        }
    }
}

void pt1_cloud_callback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg){
    pcl::fromROSMsg(*cloud_msg, pt1_cloud);
    cout << "pt1 cloud updated" << endl;
    CLOUD_UPDATED = true;
}

void wetbay_cloud_callback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg){
    cout << "cloud width: " << cloud_msg->width << " cloud height: " << cloud_msg->height << endl;
    pcl::fromROSMsg(*cloud_msg, wetbay_cloud);
    cout << "wetbay cloud updated" << endl;
    CLOUD_UPDATED = true;
}

void drybay_cloud_callback(const sensor_msgs::PointCloud2ConstPtr& cloud_msg){
    pcl::fromROSMsg(*cloud_msg, drybay_cloud);
    cout << "drybay cloud updated" << endl;
    CLOUD_UPDATED = true;
}


int main(int argc, char** argv)
{
    
    ros::init(argc, argv, "pcl_server");
    ros::NodeHandle nh;
    Server server(nh, "pcl_server", boost::bind(&executeCB, _1, &server), false);
    server.start();
    wb_pcl_client = nh.serviceClient<rc_common_msgs::Trigger>("/wetbay/rc_visard_driver/depth_acquisition_trigger");
    pt1_pcl_client = nh.serviceClient<rc_common_msgs::Trigger>("/pt1/rc_visard_driver/depth_acquisition_trigger");
    db_pcl_client = nh.serviceClient<rc_common_msgs::Trigger>("/drybay/rc_visard_driver/depth_acquisition_trigger");
    ros::Subscriber camera_sub1 = nh.subscribe("/pt1/stereo/points2", 1000, pt1_cloud_callback);
    ros::Subscriber camera_sub2 = nh.subscribe("/wetbay/stereo/points2", 1000, wetbay_cloud_callback);
    ros::Subscriber camera_sub3 = nh.subscribe("/drybay/stereo/points2", 1000, drybay_cloud_callback);
    ros::spin();

}
