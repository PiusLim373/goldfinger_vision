#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <std_msgs/Int32MultiArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <pcl/filters/passthrough.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <tf2_msgs/TFMessage.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Quaternion.h>
#include <math.h>
#include <actionlib/server/simple_action_server.h>
#include <pcl_server/PCLServerAction.h>
#include <rc_common_msgs/TriggerRequest.h>
#include <iostream>
#include <string>

using namespace std;

typedef actionlib::SimpleActionServer<pcl_server::PCLServerAction> Server;


// function to convert camera based posestamped to world base posestamped
geometry_msgs::PoseStamped transform_frame(geometry_msgs::PoseStamped Pose, string module){
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);
    cout<< "looking up " << module << "_camera" << endl;
    geometry_msgs::TransformStamped transform_stamped = tfBuffer.lookupTransform(module+"_camera", "world", ros::Time(0), ros::Duration(1.0));
    cout << transform_stamped <<endl;
    geometry_msgs::PoseStamped TransformedPose;
    tfBuffer.transform(Pose, TransformedPose, "world");
    return(TransformedPose);
}


pcl::PointCloud<pcl::PointXYZ> pt1_cloud;
pcl::PointCloud<pcl::PointXYZ> wetbay_cloud;
tf2_msgs::TFMessage tf_static;




void executeCB(const pcl_server::PCLServerGoalConstPtr &goal, Server* as)
{
    pcl_server::PCLServerResult result;
    if (goal->task == goal->UPDATE_POINTCLOUD){
        sensor_msgs::PointCloud2ConstPtr cloud_msg;
        if (goal->module.compare("wetbay") == 0){
            cout<< "updating wetbay pointcloud" << endl;
            cloud_msg = ros::topic::waitForMessage<sensor_msgs::PointCloud2> ("/wetbay/stereo/points2", ros::Duration (5.0));
            pcl::fromROSMsg(*cloud_msg, wetbay_cloud);
            result.success = true;
            as->setSucceeded(result);
        }
        else if (goal->module.compare("pt1") == 0){
            cout<< "updating pt1 cloud" << endl;
            cloud_msg = ros::topic::waitForMessage<sensor_msgs::PointCloud2> ("/pt1/stereo/points2", ros::Duration (5.0));
            pcl::fromROSMsg(*cloud_msg, pt1_cloud);
            result.success = true;
            as->setSucceeded(result);

        }
        else{
            cout<< "invalid module" << endl;
            result.success = false;
            as->setSucceeded(result);
        }
        
    }
    else if(goal->task == goal->GET_COOR){
        cout<<"getcoor"<<endl;
        // ROS_INFO("I heard %d", goal->ai_output[0]);
        
        
        geometry_msgs::PoseStamped coor_wrt_camera;
        geometry_msgs::PoseStamped coor_wrt_world;
        

        sensor_msgs::PointCloud2ConstPtr cloud_msg;
        pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
        pcl::PointXYZ point;

        cloud_msg = ros::topic::waitForMessage<sensor_msgs::PointCloud2> ("/pt1/stereo/points2", ros::Duration (5.0));
        pcl::fromROSMsg(*cloud_msg, pcl_cloud);

        int u = goal->pcl_input[0];
        int v = goal->pcl_input[1];
        int theta = goal->pcl_input[2];
        cout << goal-> module << endl;
        
        
        point = pcl_cloud.at(u, v);


        ROS_INFO("x: [%f]; y: [%f]; z: [%f]", point.x, point.y, point.z);
        if (isnan(point.x) || isnan(point.y) || isnan(point.z)){
            ROS_INFO("couldn't determine depth");
            result.success = false;
            as->setSucceeded(result);
        }
        else{
            coor_wrt_camera.header.stamp = ros::Time::now();
            coor_wrt_camera.header.frame_id = goal-> module +"_camera";
            coor_wrt_camera.pose.position.x = point.x;
            coor_wrt_camera.pose.position.y = point.y;
            coor_wrt_camera.pose.position.z = point.z;
            tf2::Quaternion quat;
            cout << coor_wrt_camera << endl;
            coor_wrt_world = transform_frame(coor_wrt_camera, goal->module);
            
            quat.setRPY(180* M_PI / 180, 0, (-(-90 + theta)+180) * M_PI / 180);
            coor_wrt_world.pose.orientation.x = quat.x();
            coor_wrt_world.pose.orientation.y = quat.y();
            coor_wrt_world.pose.orientation.z = quat.z();
            coor_wrt_world.pose.orientation.w = quat.w();

            result.coor_wrt_world = coor_wrt_world;
            cout << coor_wrt_world;
            result.success = true;
            as->setSucceeded(result);
        }
    }
    
    
}

int main(int argc, char** argv)
{
    
    ros::init(argc, argv, "pcl_server");
    ros::NodeHandle nh;
    Server server(nh, "pcl_server", boost::bind(&executeCB, _1, &server), false);
    server.start();
    
    ros::spin();

}
