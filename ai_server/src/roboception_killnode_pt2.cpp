#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/Image.h>
#include <chrono>
using namespace std;

bool PT2_NODE = true;

auto PT2_BEGIN_TIME = chrono::system_clock::now();

int PT2_NULL_COUNTER = 0;

int main(int argc, char **argv){
    ros::init(argc, argv, "roboception_killnode_pt2");
    ros::NodeHandle nh;
    while(ros::ok() && PT2_NODE){

        auto current_time = chrono::system_clock::now();
        std::chrono::duration<double> diff;

        sensor_msgs::ImageConstPtr image_ptr;
        
        diff = current_time - PT2_BEGIN_TIME;
        if(diff.count() > 10.0 && PT2_NODE){
            cout << "querying pt2 node" << endl;
            image_ptr = ros::topic::waitForMessage<sensor_msgs::Image>("/pt2/stereo/left/image_rect_color", ros::Duration(5.0));
            if(image_ptr == NULL ){
                PT2_NULL_COUNTER ++;
                cout << "pt2 image query returns NULL for " << PT2_NULL_COUNTER << " times" << endl;
                if (PT2_NULL_COUNTER >= 3){
                    cout << "couldn't get pt2 image for 3 times, pt2 rc driver might be dead, killing" << endl;;
                    system("rosnode kill /pt2/rc_visard_driver");
                    PT2_NODE = false;
                }  
            }
            else PT2_NULL_COUNTER = 0;
            PT2_BEGIN_TIME = chrono::system_clock::now();
        }

    }
}