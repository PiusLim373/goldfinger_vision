#include <ros/ros.h>
#include <iostream>
#include <sensor_msgs/Image.h>
#include <chrono>
using namespace std;

bool WETBAY_NODE = true;
bool DRYBAY_NODE = true;
bool PT1_NODE = true;

auto WB_BEGIN_TIME = chrono::system_clock::now();
auto DB_BEGIN_TIME = chrono::system_clock::now();
auto PT1_BEGIN_TIME = chrono::system_clock::now();

int WETBAY_NULL_COUNTER = 0;
int DRYBAY_NULL_COUNTER = 0;
int PT1_NULL_COUNTER = 0;

int main(int argc, char **argv){
    ros::init(argc, argv, "roboception_killnode");
    ros::NodeHandle nh;
    while(ros::ok() && (WETBAY_NODE || DRYBAY_NODE || PT1_NODE)){

        auto current_time = chrono::system_clock::now();
        std::chrono::duration<double> diff;

        sensor_msgs::ImageConstPtr image_ptr;
        
        diff = current_time - WB_BEGIN_TIME;
        if(diff.count() > 10.0 && WETBAY_NODE){
            cout << "querying wetbay node" << endl;
            image_ptr = ros::topic::waitForMessage<sensor_msgs::Image>("/wetbay/stereo/left/image_rect_color", ros::Duration(5.0));
            if(image_ptr == NULL ){
                WETBAY_NULL_COUNTER ++;
                cout << "wetbay image query returns NULL for " << WETBAY_NULL_COUNTER << " times" << endl;
                if (WETBAY_NULL_COUNTER >= 3){
                    cout << "couldn't get wetbay image for 3 times, wetbay rc driver might be dead, killing" << endl;;
                    system("rosnode kill /wetbay/rc_visard_driver");
                    WETBAY_NODE = false;
                }  
            }
            else WETBAY_NULL_COUNTER = 0;
            WB_BEGIN_TIME = chrono::system_clock::now();
        }

        diff = current_time - DB_BEGIN_TIME;
        if(diff.count() > 10.0 && DRYBAY_NODE){
            cout << "querying drybay node" << endl;
            image_ptr = ros::topic::waitForMessage<sensor_msgs::Image>("/drybay/stereo/left/image_rect_color", ros::Duration(5.0));
            if(image_ptr == NULL ){
                DRYBAY_NULL_COUNTER ++;
                cout << "drybay image query returns NULL for " << DRYBAY_NULL_COUNTER << " times" << endl;
                if (DRYBAY_NULL_COUNTER >= 3){
                    cout << "couldn't get drybay image for 3 times, drybay rc driver might be dead, killing" << endl;
                    system("rosnode kill /drybay/rc_visard_driver");
                    DRYBAY_NODE = false;
                }
            }
            else DRYBAY_NULL_COUNTER = 0;
            DB_BEGIN_TIME = chrono::system_clock::now();
        }

        diff = current_time - PT1_BEGIN_TIME;
        if(diff.count() > 10.0 && PT1_NODE){
            cout << "querying pt1 node" << endl;
            image_ptr = ros::topic::waitForMessage<sensor_msgs::Image>("/pt1/stereo/left/image_rect_color", ros::Duration(5.0));
            if(image_ptr == NULL){
                PT1_NULL_COUNTER ++;
                if (PT1_NULL_COUNTER >= 3){
                    cout << "pt1 image query returns NULL for " << PT1_NULL_COUNTER << " times" << endl;
                    cout << "couldn't get pt1 image for 3 times, pt1 rc driver might be dead, killing" << endl;
                    system("rosnode kill /pt1/rc_visard_driver");
                    PT1_NODE = false;
                }
            }   
            else PT1_NULL_COUNTER = 0;
            PT1_BEGIN_TIME = chrono::system_clock::now();
        }
    }
}