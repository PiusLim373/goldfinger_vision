#!/usr/bin/env python3
# coding: utf-8

PT1_BOUNDARY = {'x1': 493, 'y1': 273, 'x2': 968, 'y2': 589}
OCM_BOUNDARY = {'x1': 779, 'y1': 620, 'x2': 1029, 'y2': 795}

import rospy
import numpy as np
import cv2
import os
from time import sleep

def update_boundary(image):
  global PT1_BOUNDARY, OCM_BOUNDARY
  pt1_boundary_x, pt1_boundary_y = [], []
  ocm_boundary_x, ocm_boundary_y = [], []

  process_pt1, process_ocm = False, False
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  unique, counts = np.unique(ids, return_counts=True)
  id_dict = dict(zip(unique, counts))
  try:
    if id_dict[582] >= 3:
      process_pt1 = True
    if id_dict[373] >= 3:
      process_ocm = True
  except:
    pass

  if process_ocm or process_pt1:
    for (markerCorner, markerID) in zip(corners, ids):
      if process_pt1 and markerID == 582:
        corners = markerCorner.reshape((4, 2))
        (pt1_topLeft, pt1_topRight, pt1_bottomRight, pt1_bottomLeft) = corners
        pt1_topRight = (int(pt1_topRight[0]), int(pt1_topRight[1]))
        pt1_bottomRight = (int(pt1_bottomRight[0]), int(pt1_bottomRight[1]))
        pt1_bottomLeft = (int(pt1_bottomLeft[0]), int(pt1_bottomLeft[1]))
        pt1_topLeft = (int(pt1_topLeft[0]), int(pt1_topLeft[1]))
        cX = int((pt1_topLeft[0] + pt1_bottomRight[0]) / 2.0)
        cY = int((pt1_topLeft[1] + pt1_bottomRight[1]) / 2.0)
        pt1_boundary_x.append(cX)
        pt1_boundary_y.append(cY)
      if process_ocm and markerID == 373:
        corners = markerCorner.reshape((4, 2))
        (ocm_topLeft, ocm_topRight, ocm_bottomRight, ocm_bottomLeft) = corners
        ocm_topRight = (int(ocm_topRight[0]), int(ocm_topRight[1]))
        ocm_bottomRight = (int(ocm_bottomRight[0]), int(ocm_bottomRight[1]))
        ocm_bottomLeft = (int(ocm_bottomLeft[0]), int(ocm_bottomLeft[1]))
        ocm_topLeft = (int(ocm_topLeft[0]), int(ocm_topLeft[1]))
        cX = int((ocm_topLeft[0] + ocm_bottomRight[0]) / 2.0)
        cY = int((ocm_topLeft[1] + ocm_bottomRight[1]) / 2.0)
        ocm_boundary_x.append(cX)
        ocm_boundary_y.append(cY)
    if process_pt1:
      PT1_BOUNDARY = {'x1': min(pt1_boundary_x) - 15, 'y1': min(pt1_boundary_y) + 40, 'x2': max(pt1_boundary_x) + 15, 'y2': max(pt1_boundary_y) - 40}
    if process_ocm:
      OCM_BOUNDARY = {'x1': min(ocm_boundary_x) - 20, 'y1': min(ocm_boundary_y) - 53, 'x2': max(ocm_boundary_x) + 20, 'y2': max(ocm_boundary_y) + 53}
    return True
  else:
    try:
      ids = ids.flatten()
      for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv2.circle(image, (cX, cY), 5, (0, 0, 255), -1)
        cv2.imwrite("/home/ur10controller/ai_outputs/pt1/problematic_marker.jpg", image)
    except:
      pass
    print("something is wrong, found "+ str(len(corners))+ " marker")
    return False

def filter_coords(orginal_coords, location, name, u, v):
  #filter coords based on pt1 location
  filtered_coords = []
  if location == "pt1":
    for x in orginal_coords:
      if not x[1] in range(PT1_BOUNDARY['x1'], PT1_BOUNDARY['x2']) or not x[2] in range(PT1_BOUNDARY['y1'], PT1_BOUNDARY['y2']):
        pass
      else:
        filtered_coords.append(x)
  elif location == "flipper":
    for x in orginal_coords:
      if not x[1] in range(OCM_BOUNDARY['x1'], OCM_BOUNDARY['x2']) or not x[2] in range(OCM_BOUNDARY['y1'], OCM_BOUNDARY['y2']):
        pass
      else:
        filtered_coords.append(x)

  #filter coords based on previous picked item
  if name and u and v:
    for x in filtered_coords:
      if (x[0] == name) and (abs(x[1] - u) < 20) and (abs(x[2] - v) < 20):
        filtered_coords.remove(x)
  
  #sort coords based on probability
  filtered_coords.sort(key=lambda probability:probability[5], reverse=True)

  return filtered_coords

def is_too_near_boundary(u, v, location, must_return = False, default_direction = ""):
  y_softcap, x_softcap = 40, 50
  if location == 'pt1':
    compare_list = [abs(u-PT1_BOUNDARY['x1']), abs(u-PT1_BOUNDARY['x2']), abs(v-PT1_BOUNDARY['y1']), abs(v-PT1_BOUNDARY['y2'])]
    if must_return:
      print(compare_list)
      if compare_list[0] <= x_softcap:
        return 'right'
      elif compare_list[1] <= x_softcap:
        return 'left'
      elif compare_list[2] <= y_softcap:
        return 'top' #btm
      elif compare_list[3] <= y_softcap:
        return 'btm' #top
      else:
        return default_direction
    else:
      if compare_list[2] < y_softcap:
        return 'top' #btm
      elif compare_list[3] < y_softcap:
        return 'btm' #top
      elif compare_list[0] < x_softcap:
        return 'right'
      elif compare_list[1] < x_softcap:
        return 'left'
      else:
        return False

def check_inspection_bay_status(right_ib):
  right_ib_ready = right_ib.ready
  right_ib_processing = right_ib.inst_last_load

  if not right_ib_ready:
    right_ib_last_load_time = right_ib.time_last_load
    if  right_ib_last_load_time > 30:
      right_ib_ready = 1
    else:
      right_ib_ready = False
  return [right_ib_ready, right_ib_processing]

def check_force_isolation_criteria(coords, isolated_inst):
  #no need separation if more than 3 isolated inst alr
  if len(isolated_inst) >= 3:
    print("more than 3 isolated instrument available, wait instead")
    return 3
  else:
    for x in isolated_inst:
      coords.remove(x)
    #all tool isolated
    if not len(coords):
      print("all instrument are isolated")
      return 2
    else:
      return True

def detect_wbt(image):
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  unique, counts = np.unique(ids, return_counts=True)
  id_dict = dict(zip(unique, counts))
  try:
    if id_dict[116]:
      print("located marker")
      return True
    else:
      print("unable to locate marker")
      return False
  except:
    print("except case: unable to locate marker")
    return False

def pt1(self,result,goal,coords,input_img,image_np_with_detections):
  print("pt1 service called")
  force_isolation = False
  #check if picking is successful
  if goal.task == goal.PICK_SUCCESS_CHECK:
    try:
      for x in coords:
        if x[0] == goal.previous_tried_inst_name and (abs(x[1] - goal.previous_tried_inst_u) < 20) and (abs(x[2] - goal.previous_tried_inst_v) < 20):
          rospy.loginfo("found previous instrument on table, picking / dragging failed")
          result.item_remaining = 1
          print(result)
          self.server.set_succeeded(result)
          return True
      rospy.loginfo("cant find previous instrument on table, picking / dragging is successful")
      result.item_remaining = 0
      print(result)
      self.server.set_succeeded(result)
      return True
    except:
      rospy.loginfo("Something wrong during checking")
      result.item_remaining = 2
      print(result)
      self.server.set_succeeded(result)
      return False

  elif goal.task == goal.FORCE_ISOLATION:
    force_isolation = True

  elif goal.task == goal.PICK_FROM_FLIP:
    coords = filter_coords(coords, "flipper", '', 0, 0)
    try:
      if len(coords) == 1 or goal.instrument_needed == "ODD":
        rospy.loginfo("item located, proceed to pick")
        to_pick = coords[0]
        if to_pick[0] == "BAP" and len(coords) != 1:
          to_pick = coords[1]
        cv2.circle(image_np_with_detections, (to_pick[1], to_pick[2]), 5, (0,0,255), -1)
        text = "pick from flipper, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(to_pick[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        result.ai_output.item = to_pick[0]
        result.ai_output.u = to_pick[1]
        result.ai_output.v = to_pick[2]
        result.ai_output.theta = to_pick[4]
        result.ai_output.probability = to_pick[5]
        result.ai_output.side = to_pick[6]
        result.topick = True
      elif len(coords) > 1:
        u , v = 0, 0
        for i in range(len(coords)):
          u += coords[i][1]
          v += coords[i][2]
        to_pick = ['ODD', int(u/len(coords)), int(v/len(coords)), 'contour', 90, 0, 'F']
        cv2.circle(image_np_with_detections, (to_pick[1], to_pick[2]), 5, (0,0,255), -1)
        text = "pick from flipper, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(to_pick[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
        rospy.logwarn("saw more than 1 item on ocm, trying to transfer to processing table")
        result.ai_output.item = to_pick[0] 
        result.ai_output.u = to_pick[1]
        result.ai_output.v = to_pick[2]
        result.ai_output.theta = to_pick[4]
        result.ai_output.probability = to_pick[5]
        result.ai_output.side = to_pick[6]
        result.topick = True
      elif len(coords) == 0:
        rospy.logwarn("saw nothing on ocm, item probably dropped during transfer")
        result.topick = False
    except:
      result.topick = False
    print(result)
    cv2.imwrite("/home/ur10controller/ai_outputs/pt1/ai_view_pt1.jpg", image_np_with_detections)
    self.server.set_succeeded(result)
    return True

  elif goal.task == goal.GET_BOUNDARY_CENTER:
    if update_boundary(input_img):
      rospy.loginfo("boundary updated successfully")
      #returns x & y coor of pt1 as ai_output_u and ai_output_v
      result.ai_output.u = int(round(0.5*(PT1_BOUNDARY['x1'] + PT1_BOUNDARY['x2'])))
      result.ai_output.v = int(round(0.5*(PT1_BOUNDARY['y1'] + PT1_BOUNDARY['y2'])))
      #returns x & y coor of ocm as ai_output_h and ai_output_w
      result.ai_output.h = int(round(0.5*(OCM_BOUNDARY['x1'] + OCM_BOUNDARY['x2'])))
      result.ai_output.w = int(round(0.5*(OCM_BOUNDARY['y1'] + OCM_BOUNDARY['y2'])))
      result.topick = True
      print(result)
    else:
      rospy.logerr("boundary updated unsuccessful")
      result.ai_output.u, result.ai_output.v, result.ai_output.h, result.ai_output.w = 0, 0, 0, 0
      result.topick = False
    self.server.set_succeeded(result)
    return True

  elif goal.task == goal.DETECT_WBT:
    result.topick = detect_wbt(input_img)
    self.server.set_succeeded(result)
    return True

  ###################################################################################3  
  # elif goal.task == goal.DEBUG:
  #   rospy.loginfo("debug function running")
  #   self.server.set_succeeded(result)
  #   return True
  ####################################################################################

  #update_boundary
  if update_boundary(input_img):
    rospy.loginfo("boundary updated successfully")
  cv2.rectangle(image_np_with_detections, (PT1_BOUNDARY['x1'], PT1_BOUNDARY['y1']), (PT1_BOUNDARY['x2'], PT1_BOUNDARY['y2']), (255,0,0), 2)
  cv2.rectangle(image_np_with_detections, (OCM_BOUNDARY['x1'], OCM_BOUNDARY['y1']), (OCM_BOUNDARY['x2'], OCM_BOUNDARY['y2']), (255,0,0), 2)

  #filter out coords outside area of interest and previously tried target
  previous_tried_inst_name = goal.previous_tried_inst_name
  previous_tried_inst_u = goal.previous_tried_inst_u
  previous_tried_inst_v = goal.previous_tried_inst_v
  if previous_tried_inst_name == '':
    previous_tried_inst_name = None
    previous_tried_inst_u = None
    previous_tried_inst_v = None

  coords = filter_coords(coords, "pt1", previous_tried_inst_name, previous_tried_inst_u, previous_tried_inst_v)
  coords_without_contour = []
  for x in coords:
    coords_without_contour.append([x[0], x[1], x[2], x[4], x[5], x[6]])
  print("altered_coords: " + str(coords_without_contour))

  to_pick = []
  isolated_instrument = []
  crop_img = []
  for x in coords:
    isolated_instrument.append(x)

  for i in range(len(isolated_instrument)):
    x = isolated_instrument[i]
    input_temp = np.array(input_img)
    cv2.drawContours(input_temp, x[3], -1, (255,255,255), 17)
    ret,thresholded_img = cv2.threshold(input_temp,75,255,cv2.THRESH_BINARY)
    canny_img = cv2.Canny(thresholded_img,0, 0)
    rot_rect = ((x[1], x[2]), (56, 26), x[4])
    box = cv2.boxPoints(rot_rect) 
    box = np.int0(box)
    src_pts = box.astype("float32")
    width, height = 56, 26
    dst_pts = np.array([[0, height-1],
                        [0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warped = cv2.warpPerspective(canny_img, M, (width, height))
    area = cv2.countNonZero(warped)
    crop_img.append(warped)

    # if area not empty, put red marker, else green 
    if area <= 10:
      cv2.circle(input_img, (x[1], x[2]), 5, (0, 255, 0), -1)

    else:
      cv2.circle(input_img, (x[1], x[2]), 5, (0, 0, 255), -1)
      isolated_instrument[i] = ""

    cv2.circle(crop_img[i], (28, 13), 3, (255, 255, 255), -1)
    cv2.putText(crop_img[i], str(area), (28+5, 13), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 255, 255), 1)
    cv2.putText(input_img, str(i), (x[1]+5, x[2]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

  isolated_instrument[:] = [x for x in isolated_instrument if x]
  
  if len(crop_img):
    cv_view = None
    cv_view_list = []

    for i in range(len(crop_img)):
      cv_view_list.append(crop_img[i])
    cv_view = cv2.hconcat(cv_view_list)
    scale_percent = 150 # percent of original size
    width = int(cv_view.shape[1] * scale_percent / 100)
    height = int(cv_view.shape[0] * scale_percent / 100)
    dim = (width, height)

    cv_view = cv2.resize(cv_view, dim, interpolation = cv2.INTER_AREA)

    print(cv_view.shape, input_img.shape)
    
    y1, y2 = 50, 50 + cv_view.shape[0]
    x1, x2 = 50, 50 + cv_view.shape[1]

    for i in range(input_img.shape[2]):
      try:
        input_img[y1:y2, x1:x2, i] = cv_view
      except:
        print("list is too long to be appended into image, ignoring")

    cv2.imwrite('/home/ur10controller/ai_outputs/pt1/isolated_view_pt1.jpg', input_img)

  isolated_instrument[:] = [x for x in isolated_instrument if x]
  #original_coords = [['TCJ', u, v, [x,y,x,y...], 47, 99, 'F'], ['FOR', [x,y,x,y...], 45, 99, 'F', 47], ['BAP', [x,y,x,y...], 45, 99, 'F', 47]]
  # if isolated instrument are found, check with IB see if tip ib or flat ib are empty, if both empty, priorities the tip tool with F orientation
  print("isolated instrument detected, number of isolated instrument: ", len(isolated_instrument))
  if len(isolated_instrument) and not force_isolation:

    ################################################################### priority system
    right_ib_status = check_inspection_bay_status(goal.inspection_bay)
    
    tcj_list, rtl_list, flat_list = [], [], []
    for x in isolated_instrument:
      if x[0] == 'TCJ':
        tcj_list.append(x)
      elif x[0] == 'RTL':
        rtl_list.append(x)
      else:
        flat_list.append(x) 
          
    '''
    clear up rtl as it takes up most space and also both ib can handle it, more efficient,
    if no more rtl, focus on the flat tool as it requires flipping and is longer
    lastly the tcj
    '''

    #clear up retractor to either left or right ib
    if len(rtl_list):
      for x in rtl_list:
        if x[5] > 90 and x[6] == 'F':
          to_pick = x
          break
      if len(to_pick) == 0:
        to_pick = rtl_list[0]
      if not right_ib_status[0]:
        result.force_isolation = True
        to_pick = []


    #no more rtl, focus on flat tool instead
    elif right_ib_status[0] and len(flat_list):
      for x in flat_list:
        if x[5] > 90 and x[6] == 'F':
          to_pick = x
          break
      if len(to_pick) == 0:
        to_pick = flat_list[0]

    #no more flat or right ib is not free
    elif right_ib_status[0] and len(tcj_list):
      for x in tcj_list:
        if x[5] > 90 and x[6] == 'F':
          to_pick = x
          break
      if len(to_pick) == 0:
        to_pick = tcj_list[0]

    #both ib not free, or ib cant handle the other tool, force isolation instead
    else:
      print("right ib status", right_ib_status[0])
      print("number of tcj", len(tcj_list))
      print("number of flat", len(flat_list))
      print("number of rtl", len(rtl_list))
      result.force_isolation = True

    ################################################################### priority system ends 

    try:
      center_coordinates = (to_pick[1], to_pick[2])
      to_drag = is_too_near_boundary(to_pick[1], to_pick[2], "pt1")
      if to_drag:
        result.overlapped = True
        result.direction = to_drag
        if to_drag == 'btm':
          cv2.circle(image_np_with_detections, center_coordinates, 5, (255,0,0), -1)
          text = "isolated but drag away from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        elif to_drag == 'top':
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
          text = "isolated but drag towards from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        elif to_drag == 'left':
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
          text = "isolated but drag left, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        elif to_drag == 'right':
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
          text = "isolated but drag right, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
      
      else:
        result.overlapped = False
        cv2.circle(image_np_with_detections, center_coordinates, 5, (0,0,255), -1)
        text = "pick, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

      # print(to_pick)
      result.ai_output.item = to_pick[0]
      result.ai_output.u = to_pick[1]
      result.ai_output.v = to_pick[2]
      result.ai_output.theta = to_pick[4]
      result.ai_output.probability = to_pick[5]
      result.ai_output.side = to_pick[6]
      result.topick = True
      
    except:
      result.topick = False
    
  #when all instrument are stacking tgt or when force isolation is requested, remove all isolated inst fron coords and find the one closest to top edge, then proceed to drag
  elif (len(coords) != 0) or force_isolation:
    if force_isolation:
      print("force isolation")
      force_isolation_decision = check_force_isolation_criteria(coords, isolated_instrument)
      if force_isolation_decision == 3:
        result.item_remaining = 3
        print(result)
        self.server.set_succeeded(result)
        return False
      elif force_isolation_decision == 2:
        result.item_remaining = 2
        print(result)
        self.server.set_succeeded(result)
        return False

    else:
      print("all instrument are overlapping")
    v_compiled = []
    for x in coords:
      v_compiled.append(x[2])
    drag_top = coords[v_compiled.index(min(v_compiled))]
    drag_btm = coords[v_compiled.index(max(v_compiled))]
    
    drag_top_threshold, drag_btm_threshold = PT1_BOUNDARY['y1'], PT1_BOUNDARY['y2']

    #compare to see drag top or drag btm which one offer more tolerance
    if (drag_btm_threshold - drag_btm[2]) > (drag_top[2] - drag_top_threshold):
      to_pick = drag_btm
      #verify again the dragging direction
      result.direction = is_too_near_boundary(to_pick[1], to_pick[2], 'pt1', must_return = True, default_direction = "top") #btm
    else:
      to_pick = drag_top
      #verify again the dragging direction
      result.direction = is_too_near_boundary(to_pick[1], to_pick[2], 'pt1', must_return = True, default_direction = "btm") #top
    try:
      result.ai_output.item = to_pick[0]
      result.ai_output.u = to_pick[1]
      result.ai_output.v = to_pick[2]
      result.ai_output.theta = to_pick[4]
      result.ai_output.probability = to_pick[5]
      result.ai_output.side = to_pick[6]
      result.overlapped = True
      center_coordinates = (to_pick[1], to_pick[2])
      if result.direction == "btm":
        cv2.circle(image_np_with_detections, center_coordinates, 5, (255,0,0), -1)
        text = "drag away from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
      elif result.direction == "top":
        cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
        text = "drag towards from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
      elif result.direction == "left":
        cv2.circle(image_np_with_detections, center_coordinates, 5, (255,255,0), -1)
        text = "drag left from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
      elif result.direction == "right":
        cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,255), -1)
        text = "drag right from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
        cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2)
      else:
        result.topick = False
        raise Exception
      result.topick = True
    except:
      result.topick = False
  #nothing on processing table
  else:
    result.topick = False
  # cv2.imwrite('/home/ur10controller/footage/210713/test1/frame' + str(goal.task).zfill(4) + '_ai_view.jpg', image_np_with_detections)
  cv2.imwrite("/home/ur10controller/ai_outputs/pt1/ai_view_pt1.jpg", image_np_with_detections)
  print(result)
  self.server.set_succeeded(result)
    