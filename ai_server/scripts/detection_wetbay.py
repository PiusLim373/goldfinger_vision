#!/usr/bin/env python3
# coding: utf-8
import rospy
import cv2

def wetbay(self,result,goal,coords,image_np_with_detections):
  print("wetbay service called")
  print(coords)
  ############################################################################### actual process
  if goal.task == goal.PICK_SUCCESS_CHECK:
    instrument_duplicate_list = []
    if goal.instrument_needed == "tray":
      for x in coords:
        if x[0] == "tray1" or x[0] == "tray2":
          instrument_duplicate_list.append(x)
    else:
      for x in coords:
        if x[0] == goal.instrument_needed:
          instrument_duplicate_list.append(x)
    try:
      result.item_remaining = len(instrument_duplicate_list)
      result.topick = True
    except:
      result.topick = False
        

  else: 
    instrument_interested = []

    if goal.instrument_needed == "tray":
      for x in coords:
        if x[0] == "tray1" or x[0] == "tray2":
          instrument_interested = x
          break
    else:
      for x in coords:    
        if x[0] == goal.instrument_needed:
          instrument_interested = x
          break
    
    if len(instrument_interested): #not empty
      try:
        result.ai_output.item = instrument_interested[0]
        result.ai_output.u = instrument_interested[1]
        result.ai_output.v = instrument_interested[2]
        result.ai_output.theta = instrument_interested[3]
        result.ai_output.h = instrument_interested[4]
        result.ai_output.w = instrument_interested[5]
        result.ai_output.probability = instrument_interested[6]
        result.topick = True
      except:
        result.topick = False
    
    else:
      rospy.logwarn("cant locate item")
      result.topick = False
  
  ############################################################################### actual process ends
  try:
    cv2.circle(image_np_with_detections, (result.ai_output.u, result.ai_output.v), 5, (0,0,255), -1)
    cv2.putText(image_np_with_detections, result.ai_output.item, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
  except:
    pass
  print(result)
  cv2.imwrite("/home/ur10controller/ai_outputs/wetbay/ai_view_wetbay.jpg", image_np_with_detections)
  self.server.set_succeeded(result)
    
