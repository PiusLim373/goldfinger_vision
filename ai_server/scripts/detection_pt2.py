#!/usr/bin/env python3
# coding: utf-8
import rospy
import cv2
import numpy as np

BRAC_BOUNDARY = [[699, 243, 848, 403], [697, 447, 842, 607], [453,  348, 602, 504]]

def update_brac_boundary(image):
  global BRAC_BOUNDARY
  brac0_boundary_x, brac0_boundary_y = [], []
  brac1_boundary_x, brac1_boundary_y = [], []
  brac2_boundary_x, brac2_boundary_y = [], []
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  unique, counts = np.unique(ids, return_counts=True)
  id_dict = dict(zip(unique, counts))
  for (markerCorner, markerID) in zip(corners, ids):
    if id_dict[582] >= 3 and markerID == 582:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac0_boundary_x.append(cX)
      brac0_boundary_y.append(cY)
      BRAC_BOUNDARY[0] = [min(brac0_boundary_x), min(brac0_boundary_y), max(brac0_boundary_x), max(brac0_boundary_y)]
    
    if id_dict[116] >= 3 and markerID == 116:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac1_boundary_x.append(cX)
      brac1_boundary_y.append(cY)
      BRAC_BOUNDARY[1] = [min(brac1_boundary_x), min(brac1_boundary_y), max(brac1_boundary_x), max(brac1_boundary_y)]

    if id_dict[373] >= 3 and markerID == 373:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac2_boundary_x.append(cX)
      brac2_boundary_y.append(cY)
      BRAC_BOUNDARY[2] = [min(brac2_boundary_x), min(brac2_boundary_y), max(brac2_boundary_x), max(brac2_boundary_y)]


def pt2(self,result,goal,coords,image_np_with_detections):
  print("pt2 service called")
  print(coords)
  ############################################################################### actual process
  if goal.task == goal.PICK_SUCCESS_CHECK:
    instrument_duplicate_list = []
    for x in coords:
      if x[0] == goal.instrument_needed:
        instrument_duplicate_list.append(x)
    try:
      result.item_remaining = len(instrument_duplicate_list)
      result.topick = True
    except:
      result.topick = False
        

  else: 
    update_brac_boundary(image_np_with_detections)

    if goal.instrument_needed == "BRAC_0" or goal.instrument_needed == "BRAC_1" or goal.instrument_needed == "BRAC_2":
      try:
        result.ai_output.item = goal.instrument_needed
        result.ai_output.u = eval("BRAC_BOUNDARY[" + goal.instrument_needed[-1] + "][0]") + 55
        result.ai_output.v = eval("BRAC_BOUNDARY[" + goal.instrument_needed[-1] + "][1]") + 70
        result.topick = True
      except:
        result.topick = False
        rospy.logwarn("cant locate brac pose")

    else:
      cgp_interested = []
      bracket_interested = []
      region_interested = -1
      for x in coords:
        if x[0] == goal.instrument_needed:
          if not bracket_interested or x[6] > bracket_interested[6]:
            bracket_interested = x

      if bracket_interested:
        for i in range(len(BRAC_BOUNDARY)):
          if bracket_interested[1] >= BRAC_BOUNDARY[i][0] and bracket_interested[1] <= BRAC_BOUNDARY[i][2] and \
            bracket_interested[2] >= BRAC_BOUNDARY[i][1] and bracket_interested[2] <= BRAC_BOUNDARY[i][3]:
            region_interested = i
            break
        
        if region_interested == -1:
          print("bracket out of place")
          result.topick = False
        
        else:
          for x in coords:
            if x[0] == "CGP" and \
              x[1] >= BRAC_BOUNDARY[region_interested][0] and x[1] <= BRAC_BOUNDARY[region_interested][2] and \
              x[2] >= BRAC_BOUNDARY[region_interested][1] and x[2] <= BRAC_BOUNDARY[region_interested][3]:
              cgp_interested = x
              break
        
        if len(cgp_interested): #not empty
          try:
            result.ai_output.item = cgp_interested[0]
            result.ai_output.u = cgp_interested[1]
            result.ai_output.v = cgp_interested[2]
            result.ai_output.theta = cgp_interested[3]
            result.ai_output.h = cgp_interested[4]
            result.ai_output.w = cgp_interested[5]
            result.ai_output.probability = cgp_interested[6]
            result.topick = True
          except:
            result.topick = False

        else:
          rospy.logwarn("cant locate suitable cgp")
          result.topick = False
      
      else:
        rospy.logwarn("cant locate item")
        result.topick = False
  
  ############################################################################### actual process ends
  try:
    cv2.circle(image_np_with_detections, (result.ai_output.u, result.ai_output.v), 5, (0,255,0), -1)
    cv2.putText(image_np_with_detections, result.ai_output.item + ", ("+ str(result.ai_output.u) + ", " +str(result.ai_output.v)+")", (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
  except:
    pass
  print(result)
  cv2.imwrite("/home/ur10controller/ai_outputs/pt2/ai_view_pt2.jpg", image_np_with_detections)
  self.server.set_succeeded(result)
    
