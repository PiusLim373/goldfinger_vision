#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image, PointCloud2
import numpy as np
import cv2
from cv_bridge import CvBridge, CvBridgeError
bridge = CvBridge()
import os
import math
from time import sleep

PT1_BOUNDARY = {'x1': 493, 'y1': 273, 'x2': 968, 'y2': 589}
OCM_BOUNDARY = {'x1': 779, 'y1': 620, 'x2': 1029, 'y2': 795}
class AIServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('ai_server', AIServerAction, self.execute_continuous, False)
    self.server.start()
    self.bridge = CvBridge()

def callback_pt2(data):
  input_img = bridge.imgmsg_to_cv2(data, "bgr8")
  update_brac_boundary(input_img)

  # ret,thresholded_img = cv2.threshold(input_img,98,255,cv2.THRESH_BINARY)
  # canny_img = cv2.Canny(thresholded_img,0, 0)
  # output_img = update_boundary(input_img)
  # instrument_needed = "BRAC_1"
  for x in BRAC_BOUNDARY:
    cv2.rectangle(input_img, (x[0], x[1]), (x[2], x[3]), (255,0,0), 2)
    cv2.circle(input_img, (x[0] + 55, x[1] + 75), 5, (0,255,0), -1)

  cv2.imshow("Output",input_img)
  cv2.waitKey(3)

def callback_pt1(data):
  input_img = bridge.imgmsg_to_cv2(data, "bgr8")
  update_boundary(input_img)
  # ret,thresholded_img = cv2.threshold(input_img,98,255,cv2.THRESH_BINARY)
  # canny_img = cv2.Canny(thresholded_img,0, 0)
  # output_img = update_boundary(input_img)
  u = int(round(0.5*(PT1_BOUNDARY['x1'] + PT1_BOUNDARY['x2'])))
  v = int(round(0.5*(PT1_BOUNDARY['y1'] + PT1_BOUNDARY['y2'])))
  print(u,v)
  cv2.rectangle(input_img, (PT1_BOUNDARY['x1'], PT1_BOUNDARY['y1']), (PT1_BOUNDARY['x2'], PT1_BOUNDARY['y2']), (255,0,0), 2)
  cv2.rectangle(input_img, (OCM_BOUNDARY['x1'], OCM_BOUNDARY['y1']), (OCM_BOUNDARY['x2'], OCM_BOUNDARY['y2']), (255,0,0), 2)
  # cv2.rectangle(input_img, (BRAC3_BOUNDARY['x1'], BRAC3_BOUNDARY['y1']), (BRAC3_BOUNDARY['x2'], BRAC3_BOUNDARY['y2']), (0,255,0), 2)

  cv2.imshow("Output",input_img)
  cv2.waitKey(3)



# BRAC1_BOUNDARY = {'x1': 638, 'y1': 322, 'x2': 806, 'y2': 486}
# BRAC2_BOUNDARY = {'x1': 636, 'y1': 530, 'x2': 803, 'y2': 697}
# BRAC3_BOUNDARY = {'x1': 382, 'y1': 429, 'x2': 549, 'y2': 592}

BRAC_BOUNDARY = [[699, 243, 848, 403], [697, 447, 842, 607], [453,  348, 602, 504]]

def update_brac_boundary(image):
  global BRAC_BOUNDARY
  brac1_boundary_x, brac1_boundary_y = [], []
  brac2_boundary_x, brac2_boundary_y = [], []
  brac3_boundary_x, brac3_boundary_y = [], []
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  unique, counts = np.unique(ids, return_counts=True)
  id_dict = dict(zip(unique, counts))
  print(id_dict)
  for (markerCorner, markerID) in zip(corners, ids):
    if id_dict[582] >= 3 and markerID == 582:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac1_boundary_x.append(cX)
      brac1_boundary_y.append(cY)
      BRAC_BOUNDARY[0] = [min(brac1_boundary_x), min(brac1_boundary_y), max(brac1_boundary_x), max(brac1_boundary_y)]
    
    if id_dict[116] >= 3 and markerID == 116:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac2_boundary_x.append(cX)
      brac2_boundary_y.append(cY)
      BRAC_BOUNDARY[1] = [min(brac2_boundary_x), min(brac2_boundary_y), max(brac2_boundary_x), max(brac2_boundary_y)]

    if id_dict[373] >= 3 and markerID == 373:
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      brac3_boundary_x.append(cX)
      brac3_boundary_y.append(cY)
      BRAC_BOUNDARY[2] = [min(brac3_boundary_x), min(brac3_boundary_y), max(brac3_boundary_x), max(brac3_boundary_y)]

def update_boundary(image):
  global PT1_BOUNDARY, OCM_BOUNDARY
  pt1_boundary_x, pt1_boundary_y = [], []
  ocm_boundary_x, ocm_boundary_y = [], []

  process_pt1, process_ocm = False, False
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  unique, counts = np.unique(ids, return_counts=True)
  id_dict = dict(zip(unique, counts))
  # print(id_dict)
  try:
    if id_dict[582] >= 3:
      process_pt1 = True
    if id_dict[373] >= 3:
      process_ocm = True
  except:
    pass

  if process_ocm or process_pt1:
    for (markerCorner, markerID) in zip(corners, ids):
      if process_pt1 and markerID == 582:
        corners = markerCorner.reshape((4, 2))
        (pt1_topLeft, pt1_topRight, pt1_bottomRight, pt1_bottomLeft) = corners
        pt1_topRight = (int(pt1_topRight[0]), int(pt1_topRight[1]))
        pt1_bottomRight = (int(pt1_bottomRight[0]), int(pt1_bottomRight[1]))
        pt1_bottomLeft = (int(pt1_bottomLeft[0]), int(pt1_bottomLeft[1]))
        pt1_topLeft = (int(pt1_topLeft[0]), int(pt1_topLeft[1]))
        cX = int((pt1_topLeft[0] + pt1_bottomRight[0]) / 2.0)
        cY = int((pt1_topLeft[1] + pt1_bottomRight[1]) / 2.0)
        pt1_boundary_x.append(cX)
        pt1_boundary_y.append(cY)
      if process_ocm and markerID == 373:
        corners = markerCorner.reshape((4, 2))
        (ocm_topLeft, ocm_topRight, ocm_bottomRight, ocm_bottomLeft) = corners
        ocm_topRight = (int(ocm_topRight[0]), int(ocm_topRight[1]))
        ocm_bottomRight = (int(ocm_bottomRight[0]), int(ocm_bottomRight[1]))
        ocm_bottomLeft = (int(ocm_bottomLeft[0]), int(ocm_bottomLeft[1]))
        ocm_topLeft = (int(ocm_topLeft[0]), int(ocm_topLeft[1]))
        cX = int((ocm_topLeft[0] + ocm_bottomRight[0]) / 2.0)
        cY = int((ocm_topLeft[1] + ocm_bottomRight[1]) / 2.0)
        ocm_boundary_x.append(cX)
        ocm_boundary_y.append(cY)
    if process_pt1:
      PT1_BOUNDARY = {'x1': min(pt1_boundary_x) - 15, 'y1': min(pt1_boundary_y) + 40, 'x2': max(pt1_boundary_x) + 15, 'y2': max(pt1_boundary_y) - 40}
    if process_ocm:
      OCM_BOUNDARY = {'x1': min(ocm_boundary_x) - 20, 'y1': min(ocm_boundary_y) - 53, 'x2': max(ocm_boundary_x) + 20, 'y2': max(ocm_boundary_y) + 53}
      # OCM_BOUNDARY = {'x1': min(ocm_boundary_x), 'y1': min(ocm_boundary_y), 'x2': max(ocm_boundary_x), 'y2': max(ocm_boundary_y)}
    return True
  else:
    try:
      ids = ids.flatten()
      for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv2.circle(image, (cX, cY), 5, (0, 0, 255), -1)
        cv2.imwrite("/home/ur10controller/ai_outputs/pt1/problematic_marker.jpg", image)
    except:
      pass
    print("something is wrong, found "+ str(len(corners))+ " marker")
    return False

def is_too_near_boundary(u, v, location, must_return = False, default_direction = ""):
  y_softcap, x_softcap = 50, 50
  if location == 'pt1':
    compare_list = [abs(u-pt1_boundary['x1']), abs(u-pt1_boundary['x2']), abs(v-pt1_boundary['y1']), abs(v-pt1_boundary['y2'])]
    if must_return:
      print(compare_list)
      if compare_list[0] <= x_softcap:
        return 'right'
      elif compare_list[1] <= x_softcap:
        return 'left'
      elif compare_list[2] <= y_softcap:
        return 'btm'
      elif compare_list[3] <= y_softcap:
        return 'top'
      else:
        return default_direction
    else:
      if compare_list[2] <= y_softcap:
        return 'btm'
      elif compare_list[3] <= y_softcap:
        return 'top'
      elif compare_list[0] <= x_softcap:
        return 'right'
      elif compare_list[1] <= x_softcap:
        return 'left'
      else:
        return False
    return False

def get_depth(u, v, theta):
  angle_pixel_crosstable = {0: (0, -1), 45:(1, -1), 90:(1, 0), 135:(1, 1), 180: (0, 1)}
  closest_angle_list = []
  for x in angle_pixel_crosstable:
    closest_angle_list.append(abs(x - theta))
  closest_angle = min(closest_angle_list)
  print(closest_angle)

def CheckInspectionBayStatus(left_ib, right_ib):
  right_ib_ready = right_ib['ready']
  right_ib_processing = right_ib['inst_last_load']
  left_ib_ready = left_ib['ready']
  left_ib_processing = left_ib['inst_last_load']

  if not right_ib_ready and not left_ib_ready:
    right_ib_last_load_time = right_ib['time_last_load']
    left_ib_last_load_time = left_ib['time_last_load']
    if left_ib_last_load_time > right_ib_last_load_time and left_ib_last_load_time > 30:
      left_ib_ready = 1
    elif right_ib_last_load_time > left_ib_last_load_time and right_ib_last_load_time > 30:
      right_ib_ready = 1
    else:
      right_ib_ready, left_ib_ready = False, False
  return [right_ib_ready, right_ib_processing], [left_ib_ready, left_ib_processing]

def contour_color_compare_check_overlap():
  imc = cv2.imread('/home/piuslim373/footage/210709/test1/contour_filled.jpeg')
  im = cv2.cvtColor(imc, cv2.COLOR_BGR2GRAY)

  ret,thresholded_img = cv2.threshold(im,225,255,cv2.THRESH_BINARY)
  canny_img = cv2.Canny(thresholded_img,0, 0)


  input = cv2.imread('/home/piuslim373/frame0000.jpg')
  lower = np.array([66, 91, 0])
  upper = np.array([192, 192, 212])
  mask = cv2.inRange(imc, lower, upper)
  mask_inv = cv2.bitwise_not(mask)
  
  # coords = [['TCJ', 870, 483, 'contour', 145, 99, 'F', 839, 446, 901, 520], ['TCJ', 701, 604, 'contour', 15, 98, 'L',  675, 556, 730, 649]]
  
  
  crop_img, crop_img_2, crop_img_c = [], [], []

  for x in coords:
    crop_img.append(thresholded_img[x[8]-10:x[10]+10, x[7]-10:x[9]+10])
    crop_img_2.append(mask_inv[x[8]-10:x[10]+10, x[7]-10:x[9]+10])
    crop_img_c.append(imc[x[8]-10:x[10]+10, x[7]-10:x[9]+10])


  for i in range(len(crop_img)):
    cv2.imshow(str(i), crop_img_c[i])
    contours, hierarchy = cv2.findContours(crop_img[i], cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    area, area2 = 0, 0

    area2 = cv2.countNonZero(crop_img_2[i])

    
    for j in range(len(contours)):
      if hierarchy[0][j][3] == -1:
        cv2.drawContours(crop_img_2[i], [contours[j]], -1, (0,255,0), 1)
        area = cv2.contourArea(contours[j])

    

    print(str(i),hierarchy, len(contours), area, area2)
    print(area2 - area)
    # cv2.imshow(str(i), crop_img_2[i])
  cv2.imshow("hi", input)
  cv2.waitKey(0)

def gripper_clearance_check_overlap():
  coords = [['RTL', 716, 484, 86, 99, 'F']]
  input_img = cv2.imread('/home/piuslim373/frame0000.jpg')
  crop_img = []
  
  i = 0
  for x in coords:
    contour_filled = cv2.imread('/home/piuslim373/footage/210719/test1/0.jpg')
    ret,thresholded_img = cv2.threshold(contour_filled,98,255,cv2.THRESH_BINARY)
    canny_img = cv2.Canny(thresholded_img,0, 0)
    rot_rect = ((x[1], x[2]), (56, 26), x[4])
    box = cv2.boxPoints(rot_rect) 
    box = np.int0(box)
    src_pts = box.astype("float32")
    width, height = 56, 26
    dst_pts = np.array([[0, height-1],
                        [0, 0],
                        [width-1, 0],
                        [width-1, height-1]], dtype="float32")
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)

    warped = cv2.warpPerspective(canny_img, M, (width, height))
    area = cv2.countNonZero(warped)
    area2 = cv2.countNonZero(warped)
    # print(str(i), area)

    crop_img.append(warped)
    if area == 0:
        cv2.circle(input_img, (x[1], x[2]), 5, (0, 255, 0), -1)
        
    else:
      cv2.circle(input_img, (x[1], x[2]), 5, (0, 0, 255), -1)

    cv2.circle(crop_img[i], (28, 13), 3, (255, 255, 255), -1)
    cv2.putText(crop_img[i], str(area), (28+5, 13), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 255, 255), 1)
    cv2.putText(input_img, str(i), (x[1]+5, x[2]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

    
    i += 1
  
  cv_view = None
  cv_view_list = []

  for i in range(len(crop_img)):
    cv_view_list.append(crop_img[i])
  cv_view = cv2.hconcat(cv_view_list)

  scale_percent = 150 # percent of original size
  width = int(cv_view.shape[1] * scale_percent / 100)
  height = int(cv_view.shape[0] * scale_percent / 100)
  dim = (width, height)

  cv_view = cv2.resize(cv_view, dim, interpolation = cv2.INTER_AREA)

  print(cv_view.shape, input_img.shape)
  
  y1, y2 = 50, 50 + cv_view.shape[0]
  x1, x2 = 50, 50 + cv_view.shape[1]

  for i in range(input_img.shape[2]):
    input_img[y1:y2, x1:x2, i] = cv_view

  cv2.imshow("hi", thresholded_img)
  cv2.imshow("hi2", input_img)
  cv2.waitKey(0)

def isolated_instrument_debug(data):
  # input_img = cv2.imread("/home/piuslim373/frame0000.jpg")
  input_img = bridge.imgmsg_to_cv2(data, "bgr8")
  input_temp = np.array(input_img)
  ret,thresholded_img = cv2.threshold(input_temp,75,255,cv2.THRESH_BINARY)
  canny_img = cv2.Canny(thresholded_img,0, 0)
  cv2.imshow("hi", canny_img)
  cv2.waitKey(3)

def load_image():
  input_img = cv2.imread("/home/piuslim373/frame0000.jpg")
  xmin = 626
  xmax = 742
  ymin = 507
  ymax = 545
  x = int(xmin)
  y = int(ymin - 60*math.cos(math.pi/4))
  # cv2.circle(input_img, (x, y), 5, (0, 255, 0), -1)
  cv2.circle(input_img, (616, 524), 5, (0, 255, 0), -1)
  cv2.circle(input_img, (728, 494), 5, (0, 255, 0), -1)
  cv2.circle(input_img, (733, 512), 5, (0, 255, 0), -1)
  cv2.circle(input_img, (621, 543), 5, (0, 255, 0), -1)
  cv2.imshow("hi", input_img)
  cv2.waitKey(0)




if __name__ == "__main__":
  rospy.init_node('hhii', anonymous=True)
  rospy.Subscriber('/pt1/stereo/left/image_rect_color', Image, callback_pt1)
  rospy.spin()

  # load_image()

  # print('boundary updated')
  # isolated_instrument_debug()
  # image = cv2.imread("/home/piuslim373/frame0000.jpg")
  # update_boundary(image)
  # print(pt1_boundary)
  # print(is_too_near_boundary(812, 283, 'pt1', True, 'top'))