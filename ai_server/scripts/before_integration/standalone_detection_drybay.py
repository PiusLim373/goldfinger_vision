#!/usr/bin/env python3
# coding: utf-8

continuous = False

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import pathlib
import tensorflow as tf
import sys
import glob

tf.get_logger().setLevel('ERROR')    

gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)
# if gpus:
#   try:
#     tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=7000)])
#   except RuntimeError as e:
#     print(e)

# =========================================================================================================================================
import time
import rospy
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
import viz_utils_drybay as viz_utils
from sensor_msgs.msg import Image
import rospkg
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2
import matplotlib.pyplot as plt
import warnings
import roslib
roslib.load_manifest('ai_server')
from ai_server.msg import *
import actionlib
import math
warnings.filterwarnings('ignore')  

r = rospkg.RosPack()

PATH_TO_CONFIG = r.get_path('ai_server') + '/config/drybay/'

MODEL_NAME = 'faster_rcnn_inception_resnet_v2'
PATH_TO_MODEL_DIR = PATH_TO_CONFIG + 'object_detection/'+ MODEL_NAME
PATH_TO_SAVED_MODEL = PATH_TO_CONFIG + "/saved_model"
print('Loading model...', end='')
start_time = time.time()
# detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)
end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))
PATH_TO_LABELS = os.path.join(PATH_TO_CONFIG + 'label_map.pbtxt')
category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

coords = None

class AIServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('ai_server_drybay', AIServerDrybayAction, self.execute, False)
    self.server.start()
    self.bridge = CvBridge()
    self.input_img = None
    self.image_np_with_detections = None

  def execute(self, goal):
    print("service called")
    start_time = time.time()
    if goal.task == 5:
      rospy.logwarn("debug input_img overwrite")
      input_img = cv2.imread('/home/piuslim373/frame0000.jpg')
    else:
      img_data = rospy.wait_for_message('/drybay/stereo/left/image_rect_color', Image)
      input_img = self.bridge.imgmsg_to_cv2(img_data, "bgr8")
    image_np = np.array(input_img)
    input_tensor = tf.convert_to_tensor(image_np)
    input_tensor = input_tensor[tf.newaxis, ...]

    model_fn = detect_fn.signatures['serving_default']
    output_dict = model_fn(input_tensor)

    num_detections = int(output_dict.pop('num_detections'))
    need_detection_key = ['detection_classes','detection_boxes','detection_masks','detection_scores']
    output_dict = {key: output_dict[key][0, :num_detections].numpy()
                   for key in need_detection_key}
    
    output_dict['num_detections'] = num_detections
    
    # detection_classes should be ints.
    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)
    
    # Handle models with masks:
    if 'detection_masks' in output_dict:
      # Reframe the the bbox mask to the image size.
      detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks( tf.convert_to_tensor(output_dict['detection_masks']), output_dict['detection_boxes'], image_np.shape[0], image_np.shape[1])
      detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5, tf.uint8)
      output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
    
    image_np_with_detections = image_np.copy()
    coords = viz_utils.visualize_boxes_and_labels_on_image_array(image_np_with_detections, output_dict['detection_boxes'], output_dict['detection_classes'], output_dict['detection_scores'], category_index, instance_masks=output_dict.get('detection_masks_reframed', None), use_normalized_coordinates=True, line_thickness=8)

    result = AIServerDrybayResult()
    print(coords)

    ############################################################################### actual process
    if goal.task == goal.PLACE_SUCCESS_CHECK:
      #check if item is present
      instrument_found = []
      if goal.instrument_needed == "FOB" or goal.instrument_needed == "MIB":
        for x in coords:
          if x[0] == "FOB":
            instrument_found = x
        try:
          if abs(instrument_found[3] - 90) < 10:
            result.success = True
        except:
          result.suceess = False
  
    elif goal.task == 5:
      #debug function
      #skip for now
      result.suceess = True

    else:
      return_val = []
      ############################################################################### actual process: tray and container
      if goal.instrument_needed == "TRY" or goal.instrument_needed == "CON":
        if goal.instrument_needed == "TRY":
          for x in coords:
            if x[0] == "TRY_1" or x[0] == "TRY_2":
              return_val = x
        elif goal.instrument_needed == "CON":
          for x in coords:
            if x[0] == "CON_1" or x[0] == "CON_2":
              return_val = x
        try:
          result.ai_output.item = return_val[0]
          result.ai_output.u = return_val[1]
          result.ai_output.v = return_val[2] - int((return_val[8] - return_val[6])/2)
          result.ai_output.theta = return_val[3]
          result.ai_output.probability = return_val[4]
          result.success = True
        except:
          result.success = False

      ############################################################################### actual process: FORcep bracket
      elif goal.instrument_needed == "FOB":
        for x in coords:
          if x[0] == "FOB":
            return_val = x
        try:
          # return the position where the gripper is suppose to place for side pushing
          unew = 60 * math.cos(math.pi/4)
          vnew = 60 * math.sin(math.pi/4)
          u = int(return_val[5] + unew)
          v = int(return_val[6] - vnew)
          result.ai_output.item = return_val[0]
          result.ai_output.u = u
          result.ai_output.v = v
          result.ai_output.theta = return_val[3]
          result.ai_output.probability = return_val[4]
          result.success = True
        except:
          result.success = False

      ############################################################################### actual process: MIX bracket
      elif goal.instrument_needed == "MIB":
        for x in coords:
          if x[0] == "MIB":
            return_val = x
        try:
          # return the position where the gripper is suppose to place for side pushing
          unew = 60 * math.cos(math.pi/4)
          vnew = 60 * math.sin(math.pi/4)
          u = int(return_val[5] + unew)
          v = int(return_val[8] + vnew)
          result.ai_output.item = return_val[0]
          result.ai_output.u = u
          result.ai_output.v = v
          result.ai_output.theta = return_val[3]
          result.ai_output.probability = return_val[4]
          result.success = True
        except:
          result.success = False
      ############################################################################### actual process: ended


    end_time = time.time()
    try:
      cv2.circle(image_np_with_detections, (result.ai_output.u, result.ai_output.v), 5, (0,0,255), -1)
      cv2.putText(image_np_with_detections, result.ai_output.item, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
    except:
      pass
    print('=====')
    print("Detection complete, took: " + str(end_time-start_time) + "sec")
    print(result)
    cv2.imwrite("/home/piuslim373/ai_view_drybay.jpg", image_np_with_detections)
    self.server.set_succeeded(result)
    

if __name__ == "__main__":
  rospy.init_node('ai_server_drybay', anonymous=True)
  server = AIServer()
  rospy.spin()
