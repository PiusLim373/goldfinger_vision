#!/usr/bin/env python3
# coding: utf-8

pt1_boundary = {'x1': 445, 'y1': 300, 'x2': 890, 'y2': 780}
flipper_boundary = {'x1': 585, 'y1': 61, 'x2': 861, 'y2': 261}

import os

from rospy.names import resolve_name_without_node_name
from rospy.timer import sleep
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import pathlib
import tensorflow as tf
import sys
import glob

tf.get_logger().setLevel('ERROR')    

gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)
# if gpus:
#   try:
#     tf.config.experimental.set_virtual_device_configuration(gpus[0], [tf.config.experimental.VirtualDeviceConfiguration(memory_limit=6144)])
#   except RuntimeError as e:
#     print(e)

# =========================================================================================================================================
import time
import rospy
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
import viz_utils_pt1_mask as viz_utils
from sensor_msgs.msg import Image
import rospkg
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import cv2
import matplotlib.pyplot as plt
import warnings
import roslib
roslib.load_manifest('ai_server')
from ai_server.msg import *
import actionlib
warnings.filterwarnings('ignore')  

r = rospkg.RosPack()

PATH_TO_CONFIG = r.get_path('ai_server') + '/config/pt1/mask/'
MODEL_NAME = 'faster_rcnn_inception_resnet_v2'
PATH_TO_MODEL_DIR = PATH_TO_CONFIG + 'object_detection/'+ MODEL_NAME
PATH_TO_SAVED_MODEL = PATH_TO_CONFIG + "/saved_model"

utils_ops.tf = tf.compat.v1
tf.gfile = tf.io.gfile

print('Loading model...', end='')
start_time = time.time()
detect_fn = tf.saved_model.load(PATH_TO_SAVED_MODEL)
end_time = time.time()
elapsed_time = end_time - start_time
print('Done! Took {} seconds'.format(elapsed_time))
PATH_TO_LABELS = os.path.join(PATH_TO_CONFIG + 'labelmap.pbtxt')
category_index = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS, use_display_name=True)

coords = None

def update_boundary(image):
  global pt1_boundary
  arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
  arucoParams = cv2.aruco.DetectorParameters_create()
  (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict,parameters=arucoParams)
  boundary_x, boundary_y = [], []
  if len(corners) == 4:
    ids = ids.flatten()
    for (markerCorner, markerID) in zip(corners, ids):
      corners = markerCorner.reshape((4, 2))
      (topLeft, topRight, bottomRight, bottomLeft) = corners
      topRight = (int(topRight[0]), int(topRight[1]))
      bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
      bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
      topLeft = (int(topLeft[0]), int(topLeft[1]))
      cX = int((topLeft[0] + bottomRight[0]) / 2.0)
      cY = int((topLeft[1] + bottomRight[1]) / 2.0)
      boundary_x.append(cX)
      boundary_y.append(cY)
    pt1_boundary = {'x1': min(boundary_x) - 15, 'y1': min(boundary_y) + 50, 'x2': max(boundary_x) + 15, 'y2': max(boundary_y) - 55}
    # print(pt1_boundary)
    # os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_offset_x " + str(pt1_boundary['x1']))
    # os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_offset_y " + str(pt1_boundary['y1']))
    # os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_width " + str(pt1_boundary['x2'] - pt1_boundary['x1']))
    # os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_height " + str(pt1_boundary['y2'] - pt1_boundary['y1']))
    # sleep(1)
    return True
  else:
    try:
      ids = ids.flatten()
      for (markerCorner, markerID) in zip(corners, ids):
        corners = markerCorner.reshape((4, 2))
        (topLeft, topRight, bottomRight, bottomLeft) = corners
        topRight = (int(topRight[0]), int(topRight[1]))
        bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
        bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
        topLeft = (int(topLeft[0]), int(topLeft[1]))
        cX = int((topLeft[0] + bottomRight[0]) / 2.0)
        cY = int((topLeft[1] + bottomRight[1]) / 2.0)
        cv2.circle(image, (cX, cY), 5, (0, 0, 255), -1)
        cv2.imwrite("/home/piuslim373/problematic_marker.jpg", image)
    except:
      pass
    print("something is wrong, found "+ str(len(corners))+ " marker")
    return False


#original_coords = [['TCJ', u, v, [x,y,x,y...], 45, 99, 'F'], ['FOR', [x,y,x,y...], 45, 99, 'F'], ['BAP', [x,y,x,y...], 45, 99, 'F']]
def filter_coords(orginal_coords, location, name, u, v):

  #filter coords based on pt1 location
  filtered_coords = []
  if location == "pt1":
    for x in orginal_coords:
      if not x[1] in range(pt1_boundary['x1'], pt1_boundary['x2']) or not x[2] in range(pt1_boundary['y1'], pt1_boundary['y2']):
        pass
      else:
        filtered_coords.append(x)
  elif location == "flipper":
    for x in orginal_coords:
      if not x[1] in range(flipper_boundary['x1'], flipper_boundary['x2']) or not x[2] in range(flipper_boundary['y1'], flipper_boundary['y2']):
        pass
      else:
        filtered_coords.append(x)

  #filter coords based on previous picked item
  if name and u and v:
    for x in filtered_coords:
      if (x[0] == name) and (abs(x[1] - u) < 20) and (abs(x[2] - v) < 20):
        filtered_coords.remove(x)
  
  #sort coords based on probability
  filtered_coords.sort(key=lambda probability:probability[5], reverse=True)

  return filtered_coords

def is_too_near_boundary(u, v, location, must_return = False, default_direction = ""):
  y_softcap, x_softcap = 40, 50
  if location == 'pt1':
    compare_list = [abs(u-pt1_boundary['x1']), abs(u-pt1_boundary['x2']), abs(v-pt1_boundary['y1']), abs(v-pt1_boundary['y2'])]
    if must_return:
      print(compare_list)
      if compare_list[0] <= x_softcap:
        return 'right'
      elif compare_list[1] <= x_softcap:
        return 'left'
      elif compare_list[2] <= y_softcap:
        return 'btm'
      elif compare_list[3] <= y_softcap:
        return 'top'
      else:
        return default_direction
    else:
      if compare_list[2] < y_softcap:
        return 'btm'
      elif compare_list[3] < y_softcap:
        return 'top'
      elif compare_list[0] < x_softcap:
        return 'right'
      elif compare_list[1] < x_softcap:
        return 'left'
      else:
        return False
    return False

def CheckInspectionBayStatus(left_ib, right_ib):
  right_ib_ready = right_ib.ready
  right_ib_processing = right_ib.inst_last_load
  left_ib_ready = left_ib.ready
  left_ib_processing = left_ib.inst_last_load

  if not right_ib_ready and not left_ib_ready:
    right_ib_last_load_time = right_ib.time_last_load
    left_ib_last_load_time = left_ib.time_last_load
    if left_ib_last_load_time > right_ib_last_load_time and left_ib_last_load_time > 30:
      left_ib_ready = 1
    elif right_ib_last_load_time > left_ib_last_load_time and right_ib_last_load_time > 30:
      right_ib_ready = 1
    else:
      right_ib_ready, left_ib_ready = False, False
  return [right_ib_ready, right_ib_processing], [left_ib_ready, left_ib_processing]

def check_force_isolation_criteria(coords, isolated_inst):
  #no need separation if more than 3 isolated inst alr
  if len(isolated_inst) >= 3:
    print("more than 3 isolated instrument available, wait instead")
    return 3
  else:
    for x in isolated_inst:
      coords.remove(x)
    #all tool isolated
    if not len(coords):
      print("all instrument are isolated")
      return 2
    else:
      return True

class AIServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('ai_server', AIServerAction, self.execute, False)
    self.server.start()
    self.bridge = CvBridge()
    self.input_img = None
    self.image_np_with_detections = None
    
  def execute(self, goal):
    print("service called")
    start_time = time.time()
    if goal.task == 5:
      rospy.logwarn("debug input_img overwrite")
      input_img = cv2.imread('/home/piuslim373/frame0000.jpg')
    else:
      img_data = rospy.wait_for_message('/pt1/stereo/left/image_rect_color', Image)
      input_img = self.bridge.imgmsg_to_cv2(img_data, "bgr8")
    image_np = np.array(input_img)
    input_tensor = tf.convert_to_tensor(image_np)
    input_tensor = input_tensor[tf.newaxis, ...]

    model_fn = detect_fn.signatures['serving_default']
    output_dict = model_fn(input_tensor)

    num_detections = int(output_dict.pop('num_detections'))
    need_detection_key = ['detection_classes','detection_boxes','detection_masks','detection_scores']
    output_dict = {key: output_dict[key][0, :num_detections].numpy()
                   for key in need_detection_key}
    
    output_dict['num_detections'] = num_detections
    
    # detection_classes should be ints.
    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)
    
    # Handle models with masks:
    if 'detection_masks' in output_dict:
      # Reframe the the bbox mask to the image size.
      detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks( tf.convert_to_tensor(output_dict['detection_masks']), output_dict['detection_boxes'], image_np.shape[0], image_np.shape[1])
      detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5, tf.uint8)
      output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
    
    image_np_with_detections = image_np.copy()
    coords = viz_utils.visualize_boxes_and_labels_on_image_array(image_np_with_detections, output_dict['detection_boxes'], output_dict['detection_classes'], output_dict['detection_scores'], category_index, instance_masks=output_dict.get('detection_masks_reframed', None), use_normalized_coordinates=True, line_thickness=8)

    result = AIServerResult()
    # print("original_coords: " + str(coords))
    force_isolation = False
    #check if picking is successful
    if goal.task == goal.PICK_SUCCESS_CHECK:
      try:
        for x in coords:
          if x[0] == goal.previous_tried_inst_name and (abs(x[1] - goal.previous_tried_inst_u) < 20) and (abs(x[2] - goal.previous_tried_inst_v) < 20):
            rospy.loginfo("found previous instrument on table, picking / dragging failed")
            result.item_remaining = 1
            print(result)
            self.server.set_succeeded(result)
            return True
        rospy.loginfo("cant find previous instrument on table, picking / dragging is successful")
        result.item_remaining = 0
        print(result)
        self.server.set_succeeded(result)
        return True
      except:
        rospy.loginfo("Something wrong during checking")
        result.item_remaining = 2
        print(result)
        self.server.set_succeeded(result)
        return False

    elif goal.task == goal.FORCE_ISOLATION:
      force_isolation = True

    elif goal.task == goal.PICK_FROM_FLIP:
      coords = filter_coords(coords, "flipper", '', 0, 0)
      try:
        if len(coords) == 1:
          rospy.loginfo("item located, proceed to pick")
          to_pick = coords[0]
          cv2.circle(image_np_with_detections, (to_pick[1], to_pick[2]), 5, (0,0,255), -1)
          text = "pick from flipper, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(to_pick[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
          result.ai_output.item = to_pick[0]
          result.ai_output.u = to_pick[1]
          result.ai_output.v = to_pick[2]
          result.ai_output.theta = to_pick[4]
          result.ai_output.probability = to_pick[5]
          result.ai_output.side = to_pick[6]
          result.topick = True
        elif len(coords) > 1:
          u , v = 0, 0
          for i in range(len(coords)):
            u += coords[i][1]
            v += coords[i][2]
          to_pick = ['ODD', int(u/len(coords)), int(v/len(coords)), 'contour', 90, 0, 'F']
          cv2.circle(image_np_with_detections, (to_pick[1], to_pick[2]), 5, (0,0,255), -1)
          text = "pick from flipper, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(to_pick[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)
          rospy.logwarn("saw more than 1 item on ocm, trying to transfer to processing table")
          result.ai_output.item = to_pick[0] 
          result.ai_output.u = to_pick[1]
          result.ai_output.v = to_pick[2]
          result.ai_output.theta = to_pick[4]
          result.ai_output.probability = to_pick[5]
          result.ai_output.side = to_pick[6]
          result.topick = True
        elif len(coords) == 0:
          rospy.logwarn("saw nothing on ocm, item probably dropped during transfer")
          result.topick = False
      except:
        result.topick = False
      print(result)
      cv2.imwrite("ai_view.jpg", image_np_with_detections)
      self.server.set_succeeded(result)
      return True

    elif goal.task == goal.GET_BOUNDARY_CENTER:
      if update_boundary(input_img):
        rospy.loginfo("boundary updated successfully")
        result.ai_output.u = int(round(0.5*(pt1_boundary['x1'] + pt1_boundary['x2'])))
        result.ai_output.v = int(round(0.5*(pt1_boundary['y1'] + pt1_boundary['y2'])))
        result.topick = True
        print(result)
      else:
        rospy.logerr("boundary updated unsuccessful")
        result.ai_output.u, result.ai_output.v = 0, 0
        result.topick = False
      self.server.set_succeeded(result)
      return True

    elif goal.task == goal.CALIBRATE_EXP_WB:
      rospy.loginfo("calibrating exposure and white balance")
      if update_boundary(input_img):
        rospy.loginfo("boundary updated successfully, calibrating exposure and white balance")
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_offset_x " + str(pt1_boundary['x1']))
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_offset_y " + str(pt1_boundary['y1']))
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_width " + str(pt1_boundary['x2'] - pt1_boundary['x1']))
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_exp_height " + str(pt1_boundary['y2'] - pt1_boundary['y1']))
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_wb_ratio_red 1.201311707496643 ")
        os.system("rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_wb_ratio_blue 3.082214117050171 ")
        sleep(1)
        rospy.loginfo("white balance and exposure calibration is successful")
        result.topick = True
        print(result)
      else:
        rospy.logerr("boundary updated unsuccessful")
        result.topick = False
      self.server.set_succeeded(result)
      return True
    ###################################################################################3  
    if goal.task == 5:
      i = 0
      for i in range(len(coords)):
        x = coords[i]
        input_temp = np.array(input_img)
        if x[0] == 'RTL':
          cv2.drawContours(input_temp, x[3], -1, (255,255,255), 23)
        else:
          cv2.drawContours(input_temp, x[3], -1, (255,255,255), 17)
        cv2.imwrite("/home/piuslim373/footage/210727/test1/" +str(i) +'.jpg', input_temp)
        i += 1
      # self.server.set_succeeded(result)
      # return True
    ####################################################################################



    #update_boundary
    if update_boundary(input_img):
      rospy.loginfo("boundary updated successfully")
    cv2.rectangle(image_np_with_detections, (pt1_boundary['x1'], pt1_boundary['y1']), (pt1_boundary['x2'], pt1_boundary['y2']), (255,0,0), 2)

    

    #filter out coords outside area of interest and previously tried target
    previous_tried_inst_name = goal.previous_tried_inst_name
    previous_tried_inst_u = goal.previous_tried_inst_u
    previous_tried_inst_v = goal.previous_tried_inst_v
    if previous_tried_inst_name == '':
      previous_tried_inst_name = None
      previous_tried_inst_u = None
      previous_tried_inst_v = None

    coords = filter_coords(coords, "pt1", previous_tried_inst_name, previous_tried_inst_u, previous_tried_inst_v)
    coords_without_contour = []
    for x in coords:
      coords_without_contour.append([x[0], x[1], x[2], x[4], x[5], x[6]])
    print("altered_coords: " + str(coords_without_contour))

    to_pick = []
    isolated_instrument = []
    crop_img = []
    for x in coords:
      isolated_instrument.append(x)

    for i in range(len(isolated_instrument)):
      x = isolated_instrument[i]
      input_temp = np.array(input_img)
      cv2.drawContours(input_temp, x[3], -1, (255,255,255), 17)
      ret,thresholded_img = cv2.threshold(input_temp,75,255,cv2.THRESH_BINARY)
      canny_img = cv2.Canny(thresholded_img,0, 0)
      rot_rect = ((x[1], x[2]), (56, 26), x[4])
      box = cv2.boxPoints(rot_rect) 
      box = np.int0(box)
      src_pts = box.astype("float32")
      width, height = 56, 26
      dst_pts = np.array([[0, height-1],
                          [0, 0],
                          [width-1, 0],
                          [width-1, height-1]], dtype="float32")
      M = cv2.getPerspectiveTransform(src_pts, dst_pts)
      warped = cv2.warpPerspective(canny_img, M, (width, height))
      area = cv2.countNonZero(warped)
      crop_img.append(warped)

      # if area not empty, put red marker, else green 
      if area <= 10:
        cv2.circle(input_img, (x[1], x[2]), 5, (0, 255, 0), -1)

      else:
        cv2.circle(input_img, (x[1], x[2]), 5, (0, 0, 255), -1)
        isolated_instrument[i] = ""

      cv2.circle(crop_img[i], (28, 13), 3, (255, 255, 255), -1)
      cv2.putText(crop_img[i], str(area), (28+5, 13), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 255, 255), 1)
      cv2.putText(input_img, str(i), (x[1]+5, x[2]), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

    isolated_instrument[:] = [x for x in isolated_instrument if x]
    
    
    cv_view = None
    cv_view_list = []

    for i in range(len(crop_img)):
      cv_view_list.append(crop_img[i])
    cv_view = cv2.hconcat(cv_view_list)
    scale_percent = 150 # percent of original size
    width = int(cv_view.shape[1] * scale_percent / 100)
    height = int(cv_view.shape[0] * scale_percent / 100)
    dim = (width, height)

    cv_view = cv2.resize(cv_view, dim, interpolation = cv2.INTER_AREA)

    print(cv_view.shape, input_img.shape)
    
    y1, y2 = 50, 50 + cv_view.shape[0]
    x1, x2 = 50, 50 + cv_view.shape[1]

    for i in range(input_img.shape[2]):
      input_img[y1:y2, x1:x2, i] = cv_view

    cv2.imwrite('/home/piuslim373/ai_view2.jpg', input_img)

    isolated_instrument[:] = [x for x in isolated_instrument if x]
    #original_coords = [['TCJ', u, v, [x,y,x,y...], 47, 99, 'F'], ['FOR', [x,y,x,y...], 45, 99, 'F', 47], ['BAP', [x,y,x,y...], 45, 99, 'F', 47]]
    # if isolated instrument are found, check with IB see if tip ib or flat ib are empty, if both empty, priorities the tip tool with F orientation
    print("isolated instrument detected, number of isolated instrument: ", len(isolated_instrument))
    if len(isolated_instrument) and not force_isolation:

      ################################################################### priority system
      right_ib_status, left_ib_status = CheckInspectionBayStatus(goal.left_ib, goal.right_ib)
      
      tcj_list, rtl_list, los_list = [], [], []
      for x in isolated_instrument:
        if x[0] == 'TCJ':
          tcj_list.append(x)
        elif x[0] == 'RTL':
          rtl_list.append(x)
        else:
          los_list.append(x) 
            
      #right ib is available and there is tcj detected, load tcj as main target
      if right_ib_status[0] and len(tcj_list)!= 0:
        for x in tcj_list:
          if x[5] > 90 and x[6] == 'F':
            to_pick = x
            break
        if len(to_pick) == 0:
          to_pick = tcj_list[0]
        result.which_ib = 'right'
      
      #if left if is available and retractor is detected, load rtl as main target
      elif left_ib_status[0] and len(rtl_list)!= 0:
        for x in rtl_list:
          if x[5] > 90 and x[6] == 'F':
            to_pick = x
            break
        if len(to_pick) == 0:
          to_pick = rtl_list[0]
        result.which_ib = 'left'

      #if right ib is available but no isolated tcj, prform isolation if not all tcj has been processed
      elif right_ib_status[0] and len(tcj_list) == 0:
        force_isolation_decision = check_force_isolation_criteria(coords, isolated_instrument)
        print("i amhere")
        if right_ib_status[1] == 'TCJ' and force_isolation_decision != 2 and force_isolation_decision != 3:
          result.force_isolation = True
        # if all current has been processed or if no more tool to isolate, trasnfer loose instr if available
        elif len(los_list) != 0:
          for x in los_list:
            if x[5] > 90 and x[6] == 'F':
              to_pick = x
              break
          if len(to_pick) == 0:
            to_pick = los_list[0]
          result.which_ib = 'right'
        # transfer retractor is no loose inst is available at all
        elif len(los_list) == 0:
          for x in rtl_list:
            if x[5] > 90 and x[6] == 'F':
              to_pick = x
              break
          if len(to_pick) == 0:
            to_pick = rtl_list[0]
          result.which_ib = 'right'
      
      #if left ib is available but no isolated rtl, prform isolation if not all rtl has been processed
      elif left_ib_status[0] and len(rtl_list)== 0:
        force_isolation_decision = check_force_isolation_criteria(coords, isolated_instrument)
        if left_ib_status[1] == 'RTL' and force_isolation_decision != 2 and force_isolation_decision != 3:
          result.force_isolation = True
        # if all rtl has been processed, perform to trasnfer loose inst if available
        elif len(los_list) != 0:
          for x in los_list:
            if x[5] > 90 and x[6] == 'F':
              to_pick = x
              break
          if len(to_pick) == 0:
            to_pick = los_list[0]
          result.which_ib = 'left'
        # shouldnt have reach this section if everything goes as planned
        elif len(los_list) == 0:
          print("shouldn't reached this condition, left ib cant inspect tcj", tcj_list, rtl_list, los_list)
          result.force_isolation = True
      ################################################################### priority system ends 

      try:
        center_coordinates = (to_pick[1], to_pick[2])
        to_drag = is_too_near_boundary(to_pick[1], to_pick[2], "pt1")
        if to_drag:
          result.which_ib = ''
          result.overlapped = True
          result.direction = to_drag
          if to_drag == 'btm':
            cv2.circle(image_np_with_detections, center_coordinates, 5, (255,0,0), -1)
            text = "isolated but drag away from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          elif to_drag == 'top':
            cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
            text = "isolated but drag towards from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          elif to_drag == 'left':
            cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
            text = "isolated but drag left, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          elif to_drag == 'right':
            cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
            text = "isolated but drag right, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        
        else:
          result.overlapped = False
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,0,255), -1)
          text = "pick, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6] + ", " + str(result.which_ib) + " IB")
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2)

        # print(to_pick)
        result.ai_output.item = to_pick[0]
        result.ai_output.u = to_pick[1]
        result.ai_output.v = to_pick[2]
        result.ai_output.theta = to_pick[4]
        result.ai_output.probability = to_pick[5]
        result.ai_output.side = to_pick[6]
        result.topick = True
        
      except:
        result.topick = False
      
    #when all instrument are stacking tgt or when force isolation is requested, remove all isolated inst fron coords and find the one closest to top edge, then proceed to drag
    elif (len(coords) != 0) or force_isolation:
      if force_isolation:
        print("force isolation")
        force_isolation_decision = check_force_isolation_criteria(coords, isolated_instrument)
        if force_isolation_decision == 3:
          result.item_remaining = 3
          print(result)
          self.server.set_succeeded(result)
          return False
        elif force_isolation_decision == 2:
          result.item_remaining = 2
          print(result)
          self.server.set_succeeded(result)
          return False

      else:
        print("all instrument are overlapping")
      v_compiled = []
      for x in coords:
        v_compiled.append(x[2])
      drag_top = coords[v_compiled.index(min(v_compiled))]
      drag_btm = coords[v_compiled.index(max(v_compiled))]
      
      drag_top_threshold, drag_btm_threshold = pt1_boundary['y1'], pt1_boundary['y2']

      #compare to see drag top or drag btm which one offer more tolerance
      if (drag_btm_threshold - drag_btm[2]) > (drag_top[2] - drag_top_threshold):
        to_pick = drag_btm
        #verify again the dragging direction
        result.direction = is_too_near_boundary(to_pick[1], to_pick[2], 'pt1', must_return = True, default_direction = "btm")
      else:
        to_pick = drag_top
        #verify again the dragging direction
        result.direction = is_too_near_boundary(to_pick[1], to_pick[2], 'pt1', must_return = True, default_direction = "top")
      try:
        result.ai_output.item = to_pick[0]
        result.ai_output.u = to_pick[1]
        result.ai_output.v = to_pick[2]
        result.ai_output.theta = to_pick[4]
        result.ai_output.probability = to_pick[5]
        result.ai_output.side = to_pick[6]
        result.overlapped = True
        center_coordinates = (to_pick[1], to_pick[2])
        if result.direction == "btm":
          cv2.circle(image_np_with_detections, center_coordinates, 5, (255,0,0), -1)
          text = "drag away from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        elif result.direction == "top":
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,0), -1)
          text = "drag towards from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        elif result.direction == "left":
          cv2.circle(image_np_with_detections, center_coordinates, 5, (255,255,0), -1)
          text = "drag left from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
        elif result.direction == "right":
          cv2.circle(image_np_with_detections, center_coordinates, 5, (0,255,255), -1)
          text = "drag right from robot, " + str(to_pick[0]) + ", " + str(to_pick[4]) + "deg, " + str(to_pick[5]) + "%, " + str(x[6])
          cv2.putText(image_np_with_detections, text, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2)
        else:
          result.topick = False
          raise Exception
        result.topick = True
      except:
        result.topick = False
    #nothing on processing table
    else:
      result.topick = False
    cv2.imwrite('/home/piuslim373/footage/210713/test1/frame' + str(goal.task).zfill(4) + '_ai_view.jpg', image_np_with_detections)
    cv2.imwrite("ai_view.jpg", image_np_with_detections)
    print(result)
    end_time = time.time()
    print("time taken for detection: " + str(end_time-start_time) + "sec")
    self.server.set_succeeded(result)
    

if __name__ == "__main__":
  rospy.init_node('ai_server', anonymous=True)
  server = AIServer()
  rospy.spin()
