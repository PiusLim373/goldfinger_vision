#!/usr/bin/env python3
# coding: utf-8

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 
import tensorflow as tf


gpus = tf.config.experimental.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

import time
import rospy
from object_detection.utils import label_map_util
from object_detection.utils import ops as utils_ops
import viz_utils_drybay_ver5
import viz_utils_pt1_mask 
import viz_utils_wetbay
import viz_utils_pt2
from sensor_msgs.msg import Image
import rospkg
from cv_bridge import CvBridge
import numpy as np
import cv2
import roslib
roslib.load_manifest('ai_server')
from ai_server.msg import *
import actionlib
import detection_drybay 
import detection_pt1
import detection_wetbay
import detection_pt2

r = rospkg.RosPack()

LOAD_WETBAY = False
LOAD_DRYBAY = False
LOAD_PT1 = False
LOAD_PT2 = True
LOAD_ALL = True

if LOAD_ALL:
  LOAD_WETBAY, LOAD_DRYBAY, LOAD_PT1, LOAD_PT2 = True, True, True, True

def load_models():
  print('Loading model...')
  start_time = time.time()

  detect_fn1 = detect_fn2 = detect_fn3 = category_index1 = category_index2 = category_index3 = None

  if LOAD_WETBAY:
    print("loading wetbay model")
    PATH_TO_CONFIG3 = r.get_path('ai_server') + '/config/wetbay/'
    PATH_TO_SAVED_MODEL3 = PATH_TO_CONFIG3 + "/saved_model"
    detect_fn3 = tf.saved_model.load(PATH_TO_SAVED_MODEL3)
    PATH_TO_LABELS3 = os.path.join(PATH_TO_CONFIG3 + 'label_map.pbtxt')
    category_index3 = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS3, use_display_name=True)

  if LOAD_DRYBAY:
    print("loading drybay model")
    PATH_TO_CONFIG1 = r.get_path('ai_server') + '/config/drybay/'
    PATH_TO_SAVED_MODEL1 = PATH_TO_CONFIG1 + "/saved_model"
    detect_fn1 = tf.saved_model.load(PATH_TO_SAVED_MODEL1)
    PATH_TO_LABELS1 = os.path.join(PATH_TO_CONFIG1 + 'label_map.pbtxt')
    category_index1 = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS1, use_display_name=True)

  if LOAD_PT1:
    print("loading pt1 model")
    PATH_TO_CONFIG2 = r.get_path('ai_server') + '/config/pt1/'
    PATH_TO_SAVED_MODEL2 = PATH_TO_CONFIG2 + "/saved_model"
    detect_fn2 = tf.saved_model.load(PATH_TO_SAVED_MODEL2)
    PATH_TO_LABELS2 = os.path.join(PATH_TO_CONFIG2 + 'label_map.pbtxt')
    category_index2 = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS2, use_display_name=True)

  if LOAD_PT2:
    print("loading pt2 model")
    PATH_TO_CONFIG4 = r.get_path('ai_server') + '/config/pt2/'
    PATH_TO_SAVED_MODEL4 = PATH_TO_CONFIG4 + "/saved_model"
    detect_fn4 = tf.saved_model.load(PATH_TO_SAVED_MODEL4)
    PATH_TO_LABELS4 = os.path.join(PATH_TO_CONFIG4 + 'label_map.pbtxt')
    category_index4 = label_map_util.create_category_index_from_labelmap(PATH_TO_LABELS4, use_display_name=True)
  
  end_time = time.time()
  elapsed_time = end_time - start_time
  print('Done! Took {} seconds'.format(elapsed_time))
  
  return detect_fn1,detect_fn2,detect_fn3, detect_fn4,category_index1,category_index2, category_index3, category_index4

coords = None

class AIServer:
  def __init__(self):
    self.server = actionlib.SimpleActionServer('ai_server', AIServerAction, self.execute, False)
    self.server.start()
    self.bridge = CvBridge()
    self.input_img = None
    self.image_np_with_detections = None

  def execute(self, goal):
    start_time = time.time()
    print("service_called")

    if goal.which_module == goal.DRYBAY:
      detect_fn = ai[0]
      category_index = ai[4]
      module = '/drybay'
      viz_utils = viz_utils_drybay_ver5
      bay = detection_drybay

    elif goal.which_module == goal.PT1:
      detect_fn = ai[1]
      category_index = ai[5]
      module = '/pt1'
      viz_utils = viz_utils_pt1_mask
      bay = detection_pt1

    elif goal.which_module == goal.WETBAY:
      detect_fn = ai[2]
      category_index = ai[6]
      module = '/wetbay'
      viz_utils = viz_utils_wetbay
      bay = detection_wetbay
    
    elif goal.which_module == goal.PT2:
      detect_fn = ai[3]
      category_index = ai[7]
      module = '/pt2'
      viz_utils = viz_utils_pt2
      bay = detection_pt2

    if goal.task == goal.DEBUG:
      rospy.logwarn("debug input_img overwrite")
      input_img = cv2.imread('/home/piuslim373/frame0000.jpg')
    else:
      img_data = rospy.wait_for_message(module+'/stereo/left/image_rect_color', Image, timeout= 10)
      input_img = self.bridge.imgmsg_to_cv2(img_data, "bgr8")

    image_np = np.array(input_img)
    input_tensor = tf.convert_to_tensor(image_np)
    input_tensor = input_tensor[tf.newaxis, ...]
    model_fn = detect_fn.signatures['serving_default']
    output_dict = model_fn(input_tensor)
    num_detections = int(output_dict.pop('num_detections'))
    need_detection_key = ['detection_classes','detection_boxes','detection_masks','detection_scores']
    output_dict = {key: output_dict[key][0, :num_detections].numpy()
                   for key in need_detection_key}
    output_dict['num_detections'] = num_detections
    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)
    if 'detection_masks' in output_dict:
      detection_masks_reframed = utils_ops.reframe_box_masks_to_image_masks( tf.convert_to_tensor(output_dict['detection_masks']), output_dict['detection_boxes'], image_np.shape[0], image_np.shape[1])
      detection_masks_reframed = tf.cast(detection_masks_reframed > 0.5, tf.uint8)
      output_dict['detection_masks_reframed'] = detection_masks_reframed.numpy()
    image_np_with_detections = image_np.copy()

    coords = viz_utils.visualize_boxes_and_labels_on_image_array(image_np_with_detections, output_dict['detection_boxes'], output_dict['detection_classes'], output_dict['detection_scores'], category_index, instance_masks=output_dict.get('detection_masks_reframed', None), use_normalized_coordinates=True, line_thickness=8)

    result = AIServerResult()
    if bay == detection_drybay:
     bay.drybay(self,result, goal,coords,image_np_with_detections)
    elif bay == detection_pt1:
     bay.pt1(self,result, goal,coords,input_img,image_np_with_detections)
    elif bay == detection_wetbay:
     bay.wetbay(self,result,goal,coords,image_np_with_detections)
    elif bay == detection_pt2:
     bay.pt2(self,result,goal,coords,image_np_with_detections)

    end_time = time.time()
    print("Detection complete, took: " + str(end_time-start_time) + "sec")



if __name__ == "__main__":
  rospy.init_node('ai_server', anonymous=True)
  server = AIServer()
  ai = load_models()
  rospy.spin()