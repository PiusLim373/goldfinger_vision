#!/bin/bash
echo "sourcing conda"
source ~/anaconda3/etc/profile.d/conda.sh && conda activate tf
rosrun ai_server test_launch.py
echo "completed"