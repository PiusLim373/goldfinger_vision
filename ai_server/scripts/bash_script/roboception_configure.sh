#!/bin/bash
echo "configuring roboception cameras"

rosrun dynamic_reconfigure dynparam set /wetbay/rc_visard_driver ptp_enabled True && rosrun dynamic_reconfigure dynparam set /wetbay/rc_visard_driver camera_wb_auto true && rosrun dynamic_reconfigure dynparam set /wetbay/rc_visard_driver depth_quality F
rosrun dynamic_reconfigure dynparam set /drybay/rc_visard_driver ptp_enabled True && rosrun dynamic_reconfigure dynparam set /drybay/rc_visard_driver camera_wb_auto true && rosrun dynamic_reconfigure dynparam set /drybay/rc_visard_driver depth_quality F
rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver ptp_enabled True && rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver camera_wb_auto true && rosrun dynamic_reconfigure dynparam set /pt1/rc_visard_driver depth_quality F