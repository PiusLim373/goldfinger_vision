#!/usr/bin/env python3
# coding: utf-8

import rospy
import cv2
import math

def drybay(self,result,goal,coords,image_np_with_detections):
  print("drybay service called")
  print(coords)
  ############################################################################### actual process
  if goal.task == goal.PLACE_SUCCESS_CHECK:
    pass

  else:
    return_val = []
    ############################################################################### actual process: tray and container
    if goal.instrument_needed == "TRY" or goal.instrument_needed == "CON":
      if goal.instrument_needed == "TRY":
        for x in coords:
          if x[0] == "TRY_1" or x[0] == "TRY_2":
            return_val = x
      elif goal.instrument_needed == "CON":
        for x in coords:
          if x[0] == "CON_1" or x[0] == "CON_2":
            return_val = x
      try:
        result.ai_output.item = return_val[0]
        result.ai_output.u = return_val[1]
        result.ai_output.v = return_val[2] - int((return_val[8] - return_val[6])/2)
        result.ai_output.theta = return_val[3]
        result.ai_output.probability = return_val[4]
        result.topick = True
      except:
        result.topick = False

    ############################################################################### actual process: FORcep bracket
    elif goal.instrument_needed == "FOB":
      for x in coords:
        if x[0] == "FOB":
          return_val = x
      try:
        result.ai_output.item = return_val[0]
        result.ai_output.u = return_val[1]
        result.ai_output.v = return_val[2]
        result.ai_output.theta = return_val[3]
        result.ai_output.probability = return_val[4]
        result.topick = True
      except:
        result.topick = False

    ############################################################################### actual process: MIX bracket
    elif goal.instrument_needed == "MIB":
      for x in coords:
        if x[0] == "MIB":
          return_val = x
      try:
        result.ai_output.item = return_val[0]
        result.ai_output.u = return_val[1]
        result.ai_output.v = return_val[2]
        result.ai_output.theta = return_val[3]
        result.ai_output.probability = return_val[4]
        result.topick = True
      except:
        result.topick = False
    ############################################################################### actual process: KID bracket
    elif goal.instrument_needed == "KID":
      for x in coords:
        if x[0] == "KID":
          return_val = x
      try:
        result.ai_output.item = return_val[0]
        result.ai_output.u = return_val[1]
        result.ai_output.v = return_val[2]
        result.ai_output.probability = return_val[4]
        result.topick = True
      except:
        result.topick = False
    ############################################################################### actual process: GAL bracket
    elif goal.instrument_needed == "GAL":
      for x in coords:
        if x[0] == "GAL":
          return_val = x
      try:
        result.ai_output.item = return_val[0]
        result.ai_output.u = return_val[1]
        result.ai_output.v = return_val[2]
        result.ai_output.probability = return_val[4]
        result.topick = True
      except:
        result.topick = False
    ############################################################################### actual process: ended
    

  # end_time = time.time()
  try:
    cv2.circle(image_np_with_detections, (result.ai_output.u, result.ai_output.v), 5, (0,0,255), -1)
    cv2.putText(image_np_with_detections, result.ai_output.item, (15, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 0), 2)
  except:
    pass
  print('=====')
  print(result)
  cv2.imwrite("/home/ur10controller/ai_outputs/drybay/ai_view_drybay.jpg", image_np_with_detections)
  self.server.set_succeeded(result)
  
